package it.uniba.di.orariolezioni;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Classe relativa alle informazioni personali di ogni professore. Tramite tale classe si potranno visualizzare le informazioni
 * personali seguite dalle informazioni relative ai propri corsi. Tali informazioni sono recuperate da MainActivity.class
 */
public class Profilo extends AppCompatActivity {
    TextView nome;
    TextView cognome;
    TextView email;
    TextView telefono;
    Toolbar toolbar;

    ListView list;
    Professore profLoggato;

    public class MateriaCorso {

        private Insegnamento insegnamento;
        private String nomeCorso;

        MateriaCorso (Insegnamento insegnamento, String nomeCorso) {
            this.insegnamento = insegnamento;
            this.nomeCorso = nomeCorso;
        }

        public String getNomeCorso() {
            return nomeCorso;
        }

        public Insegnamento getInsegnamento() {
            return this.insegnamento;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profilo);

        profLoggato = (Professore) getIntent().getSerializableExtra("professore");

        nome = findViewById(R.id.nomeTesto);
        cognome = findViewById(R.id.cognomeTesto);
        email = findViewById(R.id.emailTesto);
        telefono = findViewById(R.id.telefonoTesto);

        toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView profilo = findViewById(R.id.titolo_prof);
        profilo.setVisibility(View.VISIBLE);
        nome.setText(profLoggato.getNome());
        cognome.setText(profLoggato.getCognome());
        email.setText(profLoggato.getEmail());
        telefono.setText(profLoggato.getTelefono());

        // Binding lista con xml
        list = (ListView) findViewById(R.id.list);

        ArrayList<MateriaCorso> arrayMateriaCorso = new ArrayList<MateriaCorso>();

        for (int i = 0; i < profLoggato.getCorso().size(); i++) {
            for (int j = 0; j < profLoggato.getCorso().get(i).getInsegnamento().size(); j++) {
                arrayMateriaCorso.add(new MateriaCorso(profLoggato.getCorso().get(i).getInsegnamento().get(j), profLoggato.getCorso().get(i).getNomeCorso()));
            }
        }

        // Launch adapter
        AdapterMateria adapter = new AdapterMateria(arrayMateriaCorso, this);
        list.setAdapter(adapter);
    }
}
