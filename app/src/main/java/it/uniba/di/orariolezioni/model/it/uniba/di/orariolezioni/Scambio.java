package it.uniba.di.orariolezioni;


import it.uniba.di.orariolezioni.Insegnamento;
import it.uniba.di.orariolezioni.Professore;

public class Scambio {

    public final int id;
    private Professore richiedente;
    private Professore destinatario;
    private Insegnamento insegnamento;
    private float inizio;
    private float fine;
    private Lezione.Giorni giorno;
    private boolean permanente;
    private boolean stato;

    public Scambio(int id, Professore richiedente, Professore destinatario,
                    Insegnamento insegnamento, float inizio, float fine,
                    Lezione.Giorni giorno, boolean permanente, boolean stato){

        this.id = id;
        this.richiedente = richiedente;
        this.destinatario = destinatario;
        this.insegnamento = insegnamento;
        this.inizio = inizio;
        this.fine = fine;
        this.giorno = giorno;
        this.permanente = permanente;
        this.stato = stato;
    }

    public int getId(){ return id;}
    public Professore getRichiedente(){return richiedente;}
    public Professore getDestinatario(){return destinatario;}
    public Insegnamento getInsegnamento(){return insegnamento;}
    public float getInizio(){ return inizio;}
    public float getFine(){ return fine;}
    public Lezione.Giorni getGiorno(){ return giorno;}
    public boolean getPermanente(){ return permanente;}
    public boolean getStato(){ return stato;}

}
