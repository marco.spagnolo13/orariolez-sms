package it.uniba.di.orariolezioni.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

// Classe di controllo presenza connessione per stampa errore ed evitare il crash dell'applicazione
public class CheckNetworkStatus {

    /**
     * Controlla la presenza di connettività internet tramite il ConnectivityManager
     * @return info sulla connettività
     */
    public static boolean isNetworkAvailable(Context context){
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}