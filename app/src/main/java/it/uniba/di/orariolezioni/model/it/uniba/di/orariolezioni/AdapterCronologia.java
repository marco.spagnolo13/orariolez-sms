package it.uniba.di.orariolezioni;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import it.uniba.di.orariolezioni.helper.HttpJsonParser;

/**
 * Adapter relativo alla stampa degli scambi con stato in attesa indirizzati al professore loggato. In questo adapter
 * vi è la possibilità di accettare o rifiutare uno scambio e quindi aggiornare a runtime lo stato dei fragment FragmentCronologia,
 * FragmentScambio e FragmentCalendario.
 */
public class AdapterCronologia extends BaseAdapter implements ListAdapter {

    // Interfaccia per il FragmentCronologia
    public interface AdapterInterface{
        public void buttonPressed();
    }

    private ArrayList<ListaCronologia> lista;
    private TextView[] textViews = new TextView[14];
    private ArrayList<JSONObject> listcronologia = new ArrayList<>();
    private boolean nodata;

    // Istanza del costruttore Scambio
    public class ListaCronologia{

        int IdScambio;
        Professore Richiedente;
        Professore Destinatario;
        int giornoDest;
        int meseDest;
        int annoDest;
        String oraInizioDest;
        String oraFineDest;
        Insegnamento InsegnamentoRic;
        Insegnamento InsegnamentoDes;
        int permanente;
        int stato;
        int giornoRic;
        int meseRic;
        int annoRic;
        String oraInizioRic;
        String oraFineRic;

        public ListaCronologia(int IdScambio, Professore Richiedente, Professore Destinatario, int giornoDest,
                               int meseDest, int annoDest, String oraInizioDest, String oraFineDest, Insegnamento InsegnamentoRic,
                               Insegnamento InsegnamentoDes, int permanente, int stato,
                               int giornoRic, int meseRic, int annoRic, String oraInizioRic, String oraFineRic){
            this.IdScambio = IdScambio;
            this.Richiedente = Richiedente;
            this.Destinatario = Destinatario;
            this.giornoDest = giornoDest;
            this.meseDest = meseDest;
            this.annoDest = annoDest;
            this.oraInizioDest = oraInizioDest;
            this.oraFineDest = oraFineDest;
            this.InsegnamentoRic = InsegnamentoRic;
            this.InsegnamentoDes = InsegnamentoDes;
            this.permanente = permanente;
            this.stato = stato;
            this.giornoRic = giornoRic;
            this.meseRic = meseRic;
            this.annoRic = annoRic;
            this.oraInizioRic = oraInizioRic;
            this.oraFineRic = oraFineRic;
        }
    }

    JSONArray arrayobj;
    private Context mContext;
    AdapterInterface buttonListener;

    public AdapterCronologia(JSONArray arrayobj, Context mContext, AdapterInterface buttonListener){
        this.arrayobj = arrayobj;
        this.mContext = mContext;
        this.buttonListener = buttonListener;
    }

    @Override
    public int getCount(){
        return arrayobj.length();
    }

    @Override
    public Object getItem(int pos){
        try {
            return arrayobj.get(pos);
        } catch (JSONException e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public long getItemId(int pos){
        return pos;
    }

    /**
     * Permette la visualizzazione della ListView con tutti gli elementi previsti
     * @param position posizione di ogni elemento interno alla Lista
     * @param convertView vista principale
     * @param parent parent vista principale
     * @return vista
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        View view = convertView;
        if (view == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
            nodata = checkCronologia();
            if (nodata) {
                view = inflater.inflate(R.layout.singolo_elemento_listview_cronologia, null);
            } else {
                view = inflater.inflate(R.layout.noelement,null);
            }
        }

        if (nodata) {
            // Chiamata metodo per la decodifica del JSONArray
            getCronologia();

            ImageView image;
            image = view.findViewById(R.id.imageView2);
            image.setImageResource(R.drawable.singola);

            // Link textviews con gli elementi dell'xml cronologia
            textViews[0] = view.findViewById(R.id.IdCrono);
            textViews[1] = view.findViewById(R.id.NomeCorsoRicCrono);
            textViews[2] = view.findViewById(R.id.ProfRichiedenteScambioCrono);
            textViews[3] = view.findViewById(R.id.OraInizioDestCrono);
            textViews[4] = view.findViewById(R.id.OraFineDestCrono);
            textViews[5] = view.findViewById(R.id.NomeCorsoDesCrono);
            textViews[6] = view.findViewById(R.id.ProfDestinatarioScambioCrono);
            textViews[7] = view.findViewById(R.id.GiornoDestCrono);
            textViews[8] = view.findViewById(R.id.MeseDestCrono);
            textViews[9] = view.findViewById(R.id.OraInizioRicCrono);
            textViews[10] = view.findViewById(R.id.OraFineRicCrono);
            textViews[11] = view.findViewById(R.id.GiornoRicCrono);
            textViews[12] = view.findViewById(R.id.MeseRicCrono);

            // Popolamento della ListView
            for (int i = 0; i < lista.size(); i++) {
                populateCronologia(textViews, position, lista);
            }

            Button deleteBtn = view.findViewById(R.id.delete_btn);
            Button addBtn = view.findViewById(R.id.confirm_btn);

            // Scambio rifiutato
            try {
                deleteBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            // Notifica
                            new sendnotify(String.valueOf(lista.get(position).Richiedente.getId()),
                                    mContext.getString(R.string.NotifyRefused),
                                    mContext.getString(R.string.msgNotifyRefused) + " "
                                            + lista.get(position).Destinatario.getNome() + " "
                                            + lista.get(position).Destinatario.getCognome())
                                    .execute().get();
                            // Chiamata Async eliminazione
                            new deleteAsync(String.valueOf(lista.get(position).IdScambio)).execute().get();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // Chiamata Fragment per update informazioni
                        FragmentScambio.flag = false;
                        buttonListener.buttonPressed();
                        lista.remove(position);
                        notifyDataSetChanged();
                    }
                });
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            // Scambio accettato
            try {
                addBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            // Notifica
                            new sendnotify(String.valueOf(lista.get(position).Richiedente.getId()),
                                    mContext.getString(R.string.NotifyAccepted),
                                    mContext.getString(R.string.msgNotifyAccepted) + " "
                                            + lista.get(position).Destinatario.getNome() + " "
                                            + lista.get(position).Destinatario.getCognome())
                                    .execute().get();
                            // Chiamata Async Conferma
                            new confirmAsync(String.valueOf(lista.get(position).IdScambio)).execute().get();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        // Chiamata Fragment per Update informazioni
                        FragmentScambio.flag = false;
                        FragmentCalendario.flag = false;
                        RichiestaScambio.flag = false;
                        buttonListener.buttonPressed();
                        lista.remove(position);
                        notifyDataSetChanged();
                    }
                });
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        return view;
    }

    /**
     * Metodo per il controllo della presenza di scambi in remoto
     * @return flag che indica se vi sono scambi o meno
     */
    public boolean checkCronologia(){
        boolean flag = false;
        for (int i = 0; i < arrayobj.length(); i++){
            try {
                listcronologia.add((JSONObject)arrayobj.get(i));
            } catch (JSONException e){
                e.printStackTrace();
            }
        }

        JSONObject cronologia;
        for (int i=0; i< listcronologia.size(); i++){
            try {
                cronologia = listcronologia.get(i);
                if (Integer.parseInt(cronologia.getString("risposta")) == 0){
                    flag = false;
                } else {
                    flag = true;
                }
            } catch (JSONException e){
                e.printStackTrace();
            }
        }
        return flag;
    }

    /**
     * Metodo per decodificare il JSONArray ricevuto e impostarlo in un'Arraylist
     */
    public void getCronologia(){

        // Aggiunta oggetti all'arraylist
        for (int i = 0; i < arrayobj.length(); i++){
            try {
                listcronologia.add((JSONObject)arrayobj.get(i));
            } catch (JSONException e){
                e.printStackTrace();
            }
        }

        JSONObject cronologia;
        ListaCronologia listaCronologia;
        lista = new ArrayList<>();
        Professore Richiedente;
        Professore Destinatario;
        Insegnamento InsegnamentoRic;
        Insegnamento InsegnamentoDes;

        // Divisione dei singoli elementi di ogni oggetto in ArrayList con identidicatore
        for (int i = 0; i < listcronologia.size(); i++){
            try {
                cronologia = listcronologia.get(i);
                Richiedente = new Professore(Integer.parseInt(cronologia.getString("Richiedente")),cronologia.getString("ProfRicNome"),cronologia.getString("ProfRicCognome"));
                Destinatario = new Professore(Integer.parseInt(cronologia.getString("Destinatario")),cronologia.getString("ProfDestNome"),cronologia.getString("ProfDestCognome"));
                InsegnamentoRic = new Insegnamento(Integer.parseInt(cronologia.getString("InsegnamentoRichiedente")),cronologia.getString("NomeInsegnamentoRic"));
                InsegnamentoDes = new Insegnamento(Integer.parseInt(cronologia.getString("InsegnamentoDestinatario")),cronologia.getString("NomeInsegnamentoDest"));
                listaCronologia = new ListaCronologia(Integer.parseInt(cronologia.getString("IdScambio")), Richiedente, Destinatario, Integer.parseInt(cronologia.getString("GiornoDest")),
                        Integer.parseInt(cronologia.getString("MeseDest")), Integer.parseInt(cronologia.getString("AnnoDest")), cronologia.getString("FasciaInizialeDest"),
                        cronologia.getString("FasciaFinaleDest"), InsegnamentoRic, InsegnamentoDes, Integer.parseInt(cronologia.getString("Permanente")), Integer.parseInt(cronologia.getString("Stato")),
                        Integer.parseInt(cronologia.getString("GiornoRic")),Integer.parseInt(cronologia.getString("MeseRic")),
                        Integer.parseInt(cronologia.getString("AnnoRic")),cronologia.getString("FasciaInizialeRic"),cronologia.getString("FasciaFinaleRic"));
                lista.add(listaCronologia);
            } catch (JSONException e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Metodo per il caricamento dei dati su TextViews
     * @param tv contenente le textViews dichiarate interne alla ListView
     * @param i contente la position di ogni oggetto
     * @param list contenente i dati da stampare
     */
    public void populateCronologia(TextView[] tv, Integer i, ArrayList<ListaCronologia> list){
        tv[0].setText(String.valueOf(list.get(i).IdScambio));
        tv[1].setText(String.valueOf(list.get(i).InsegnamentoRic.getNome()));
        String appendNomeR = (list.get(i).Richiedente.getNome()) + " " + (list.get(i).Richiedente.getCognome());
        tv[2].setText(appendNomeR);
        tv[3].setText(String.valueOf(list.get(i).oraInizioDest));
        tv[4].setText(String.valueOf(list.get(i).oraFineDest));
        tv[5].setText(String.valueOf(list.get(i).InsegnamentoDes.getNome()));
        String appendNomeD = (list.get(i).Destinatario.getNome()) + " " + (list.get(i).Destinatario.getCognome());
        tv[6].setText(appendNomeD);
        tv[7].setText(String.valueOf(list.get(i).giornoDest));
        String appendMese = String.valueOf(list.get(i).meseDest);
        switch (appendMese) {
            case ("1"): tv[8].setText(R.string.gennaio);break;
            case ("2"): tv[8].setText(R.string.febbraio);break;
            case ("3"): tv[8].setText(R.string.marzo);break;
            case ("4"): tv[8].setText(R.string.aprile);break;
            case ("5"): tv[8].setText(R.string.maggio);break;
            case ("6"): tv[8].setText(R.string.giugno);break;
            case ("7"): tv[8].setText(R.string.luglio);break;
            case ("8"): tv[8].setText(R.string.agosto);break;
            case ("9"): tv[8].setText(R.string.settembre);break;
            case ("10"): tv[8].setText(R.string.ottobre);break;
            case ("11"): tv[8].setText(R.string.novembre);break;
            case ("12"): tv[8].setText(R.string.dicembre);break;
        }
        tv[9].setText(String.valueOf(list.get(i).oraInizioRic));
        tv[10].setText(String.valueOf(list.get(i).oraFineRic));
        tv[11].setText(String.valueOf(list.get(i).giornoRic));
        String appendMeseRic = String.valueOf(list.get(i).meseRic);
        switch (appendMeseRic) {
            case ("1"): tv[12].setText(R.string.gennaio);break;
            case ("2"): tv[12].setText(R.string.febbraio);break;
            case ("3"): tv[12].setText(R.string.marzo);break;
            case ("4"): tv[12].setText(R.string.aprile);break;
            case ("5"): tv[12].setText(R.string.maggio);break;
            case ("6"): tv[12].setText(R.string.giugno);break;
            case ("7"): tv[12].setText(R.string.luglio);break;
            case ("8"): tv[12].setText(R.string.agosto);break;
            case ("9"): tv[12].setText(R.string.settembre);break;
            case ("10"): tv[12].setText(R.string.ottobre);break;
            case ("11"): tv[12].setText(R.string.novembre);break;
            case ("12"): tv[12].setText(R.string.dicembre);break;
        }
    }

    /**
     * Classe per inviare a database remoto la conferma di uno scambio
     */
    public static class confirmAsync extends AsyncTask<Void, Void, JSONArray>{

        private final String CONFIRM = "http://www.orariolezionisms.altervista.org/postConfirmScambio.php";
        Map<String,String> mappa = new HashMap<>();

        private confirmAsync(String...params){
            mappa.put("IdScambio",params[0].trim());
        }

        @Override
        protected JSONArray doInBackground(Void...params){
            try {
                HttpJsonParser parser = new HttpJsonParser();
                JSONArray jObj;
                jObj = parser.makeHttpRequest(CONFIRM,"GET",mappa);
                return jObj;
            } catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * Classe per inviare a database remoto la negazione di uno scambio
     */
    public static class deleteAsync extends AsyncTask<Void, Void, JSONArray>{

        private final String DELETE = "http://www.orariolezionisms.altervista.org/postDeleteScambio.php";
        Map<String,String> mappa = new HashMap<>();

        private deleteAsync(String...params){
            mappa.put("IdScambio",params[0].trim());
        }

        @Override
        protected JSONArray doInBackground(Void...params){
            try {
                HttpJsonParser parser = new HttpJsonParser();
                JSONArray jObj;
                jObj = parser.makeHttpRequest(DELETE,"GET",mappa);
                return jObj;
            } catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * Classe per mandare la notifica al prof interessato
     */
    public static class sendnotify extends AsyncTask<Void, Void, JSONArray>{

        private final String CONFIRM = "http://www.orariolezionisms.altervista.org/postnot.php";
        Map<String, String> mappa = new HashMap<>();

        private sendnotify(String... param){
            mappa.put("IdProfessore", param[0].trim());
            mappa.put("Titolo", param[1]);
            mappa.put("Messaggio", param[2]);
        }

        @Override
        protected JSONArray doInBackground(Void...params){
            try {
                HttpJsonParser parser = new HttpJsonParser();
                JSONArray jObj;
                jObj = parser.makeHttpRequest(CONFIRM,"GET",mappa);
                return jObj;
            } catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }
    }
}
