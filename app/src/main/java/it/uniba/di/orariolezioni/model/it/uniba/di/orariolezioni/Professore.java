package it.uniba.di.orariolezioni;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

public  class Professore implements Serializable {

    private final int id;
    private String nome;
    private String cognome;
    private String email;
    private String telefono;
    private String stanza;
    private String ruolo;
    private ArrayList<CorsoLaurea> corso;

    public Professore(int id,String nome,String cognome,ArrayList<CorsoLaurea> corso){
        this.id = id;
        this.nome = nome;
        this.cognome = cognome;
        this.corso = corso;
    }

    public Professore(final int id,String email,String nome,String cognome,String telefono,String stanza,String ruolo,ArrayList<CorsoLaurea> corso){
        this.id = id;
        this.email = email;
        this.nome =  nome;
        this.cognome = cognome;
        this.telefono = telefono;
        this.stanza = stanza;
        this.ruolo  = ruolo;
        this.corso = corso;
    }

    public Professore(int id,String nome,String cognome){
        this.id = id;
        this.nome = nome;
        this.cognome = cognome;
        this.corso = new ArrayList<CorsoLaurea>();
    }

    // Metodi getter
    public int getId(){ return id; }
    public String getNome(){ return nome; }
    public String getEmail(){ return email;}
    public String getCognome(){ return cognome; }
    public String getTelefono(){ return telefono; }
    public String getStanza() { return stanza; }
    public String getRuolo() { return ruolo; }
    public ArrayList<CorsoLaurea> getCorso(){
        return corso;
    }

    public boolean equals(Professore prof){
        if(id == prof.id){
            return true;
        } else {
            return false;
        }
    }
}
