package it.uniba.di.orariolezioni;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import it.uniba.di.orariolezioni.helper.HttpJsonParser;

/**
 * Classe principale di tutto il programma. Tramite questa classe vengono recuperati tutti i dati relativi al professore
 * loggato utilizzando metodi che estendono AsyncTask. Vi è inoltre la gestione del drawer.
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private int[] imageResId = {
            R.drawable.ic_compare_arrows,
            R.drawable.ic_date_range,
            R.drawable.ic_notifications,
            R.drawable.ic_add_circle_outline
    };

    private final String GETDATIPERSONALI = "http://www.orariolezionisms.altervista.org/getDatiPersonali.php";
    private Professore profLoggato;

    // Recupera i dati da database remoto
    private class Risposta extends AsyncTask<Void, Void, JSONArray> {

        Map<String, String> mappa = new HashMap<>();

        private Risposta(String... param) {
            mappa.put("IdProfessore", param[0].trim());
        }

        @Override
        protected JSONArray doInBackground(Void... param) {
            try {
                HttpJsonParser parser = new HttpJsonParser();
                JSONArray jObj;
                jObj = parser.makeHttpRequest(GETDATIPERSONALI, "GET", mappa);
                return jObj;
            } catch (Exception e) {
                return null;
            }
        }
    }

    private ActionBarDrawerToggle toggle;
    private DrawerLayout drawer;
    private String mActivityTitle; //stringa per cambiare il titolo dell'action bar
    private Toolbar toolbar;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private NavigationView navigationView;
    private ImageView iconaCalendario;
    private CalendarView vistaCalendario;
    private int year;
    private static int month = 0;
    private static int day = 0;
    private TextView giorno;
    SharedPreferences preferenzecondivise;
    public static final String PREFERENZE = "PreferenzePersonali";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferenzecondivise = getSharedPreferences(PREFERENZE, Context.MODE_PRIVATE);
        NotificationManager notificationmanager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        tabLayout = findViewById(R.id.TabLayout);
        viewPager = findViewById(R.id.pager);
        PageAdapter myPagerAdapter = new PageAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), this);
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(myPagerAdapter);
        viewPager.setCurrentItem(1);
        tabLayout.bringToFront();
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        setupTabIcons();

        FirebaseMessaging.getInstance().subscribeToTopic("global");
        // Deprecato ma inutile
        String notificationToken = FirebaseInstanceId.getInstance().getToken();

        //Toolbar e drawer layout
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        giorno = findViewById(R.id.giorno);
        Calendar calendario = Calendar.getInstance();
        int mese = calendario.get(Calendar.MONTH);

        if (mese < 7) {
            String semestre = "2";
            SharedPreferences.Editor editor = preferenzecondivise.edit();
            editor.putString("semestre", semestre);
            editor.commit();
        } else {
            String semestre = "1";
            SharedPreferences.Editor editor = preferenzecondivise.edit();
            editor.putString("semestre", semestre);
            editor.commit();
        }

        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);

        // Operazioni drawer
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        String id = preferenzecondivise.getString("IdProfessore", String.valueOf(0));
        Risposta risp = new Risposta(id);
        JSONArray obj = null;

        try {
            obj = risp.execute().get();
            getInfoProfessore(obj);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.home) {
            drawer.openDrawer(Gravity.LEFT);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;
        switch (id) {
            case R.id.profilo:
                intent = new Intent(this, Profilo.class);
                intent.putExtra("professore", getProfLoggato());
                startActivityForResult(intent, 0);
                break;
            case R.id.home:
                if (drawer.isDrawerOpen(Gravity.START)) {//or other check
                    drawer.openDrawer(Gravity.START);
                } else {
                    drawer.closeDrawer(Gravity.START);
                }
                break;

            case R.id.logout:
                SharedPreferences.Editor editor = preferenzecondivise.edit();
                editor.putBoolean("Loggato", false);
                editor.commit();
                intent = new Intent(this, Login.class);
                startActivity(intent);
                finish();
                break;
        }
        item.setChecked(true);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setupTabIcons() {
        try {
            tabLayout.getTabAt(0).setIcon(imageResId[0]);
            tabLayout.getTabAt(1).setIcon(imageResId[1]);
            tabLayout.getTabAt(2).setIcon(imageResId[2]);
            tabLayout.getTabAt(3).setIcon(imageResId[3]);
        } catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_drawer, menu);
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        navigationView.getMenu().getItem(0).setChecked(false);
        Risposta risp = new Risposta(preferenzecondivise.getString("IdProfessore", String.valueOf(0)));
        JSONArray obj = null;

        try {
            obj = risp.execute().get();
            getInfoProfessore(obj);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo per recuperare le materie del professore da db remoto
     * @param object JSONArray contenente le materie con relative informazioni
     */
    public void getInfoProfessore(JSONArray object) {

        JSONObject professore = null;
        ArrayList<JSONObject> lista = new ArrayList<>();

        try {
            professore = (JSONObject) object.getJSONObject(0);
            JSONArray elenco = professore.getJSONArray("Materia");
            for (int i = 0; i < elenco.length(); i++) {
                lista.add((JSONObject) elenco.get(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONObject prof = null;
        Insegnamento insegnamento;

        ArrayList<CorsoLaurea> corso = new ArrayList<>();
        CorsoLaurea cL;

        for (int i = 0; i < lista.size(); i++) {
            try {
                prof = lista.get(i);
                insegnamento = new Insegnamento(Integer.parseInt(prof.getString("IdInsegnamento")), prof.getString("Nome"), prof.getString("Anno"),
                        Integer.parseInt(prof.getString("Cfu")),
                        prof.getString("Semestre"), prof.getString("Gruppo"), prof.getString("Colore"),prof.getString("Sigla"));
                if (corso.size() == 0) {
                    cL = new CorsoLaurea(Integer.parseInt(prof.getString("IdCorsoLaurea")), prof.getString("NomeCorso"));
                    corso.add(cL);
                    corso.get(0).setInsegnamento(insegnamento);
                } else if (corso.size() == 1 && Integer.parseInt(prof.getString("IdCorsoLaurea")) == corso.get(0).getIdCorso()) {
                    corso.get(0).setInsegnamento(insegnamento);
                } else if (corso.size() == 1 && Integer.parseInt(prof.getString("IdCorsoLaurea")) != corso.get(0).getIdCorso()) {
                    cL = new CorsoLaurea(Integer.parseInt(prof.getString("IdCorsoLaurea")), prof.getString("NomeCorso"));
                    corso.add(cL);
                    corso.get(1).setInsegnamento(insegnamento);
                } else if (corso.size() > 1 && Integer.parseInt(prof.getString("IdCorsoLaurea")) == corso.get(0).getIdCorso()) {
                    corso.get(0).setInsegnamento(insegnamento);
                } else if (corso.size() > 1 && Integer.parseInt(prof.getString("IdCorsoLaurea")) == corso.get(1).getIdCorso()) {
                    corso.get(1).setInsegnamento(insegnamento);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try {
            profLoggato = new Professore(Integer.parseInt(professore.getString("IdProfessore")), professore.getString("Email"),
                    professore.getString("Nome"), professore.getString("Cognome"), professore.getString("Telefono"),
                    professore.getString("Stanza"), professore.getString("Ruolo"), corso);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Professore getProfLoggato() {
        return profLoggato;
    }
}
