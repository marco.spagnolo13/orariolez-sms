package it.uniba.di.orariolezioni;

import java.util.Calendar;
//classe che indica per ogni insegnamento quante ore vengono fatte
public class Orario {

    private String orainizio;
    private String orafine;
    private Insegnamento insegnamento;
    private Calendar data;
    private String aula;

    public Orario(String orainizio,String orafine,Insegnamento insegnamento,Calendar data,String aula){

        this.orainizio = orainizio;
        this.orafine = orafine;
        this.insegnamento = insegnamento;
        this.data = data;
        this.aula = aula;
    }

    // Metodi getter
    public String getOrainizio(){return orainizio;}
    public String getOrafine(){return orafine;}
    public Insegnamento getInsegnamento(){return insegnamento;}
    public Calendar getData(){return data;}
    public String getAula(){return aula;}
}
