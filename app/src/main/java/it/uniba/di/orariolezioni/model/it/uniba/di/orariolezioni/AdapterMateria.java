package it.uniba.di.orariolezioni;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterMateria extends BaseAdapter implements ListAdapter {

    TextView[] textViews = new TextView[6];
    ArrayList<Profilo.MateriaCorso> arrayMateriaCorso;
    private Context mContext;

    public AdapterMateria(ArrayList<Profilo.MateriaCorso> arrayMateriaCorso, Context mContext) {
        this.arrayMateriaCorso = arrayMateriaCorso;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return arrayMateriaCorso.size();
    }

    @Override
    public Object getItem(int pos) {
        return arrayMateriaCorso.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.singolo_elemento_listview_materia, null);
        }

        // Link textviews con xml
        textViews[0] = view.findViewById(R.id.nomeInsegnamento);
        textViews[1] = view.findViewById(R.id.nomeAnno);
        textViews[2] = view.findViewById(R.id.nomeCfu);
        textViews[3] = view.findViewById(R.id.nomeSemestre);
        textViews[4] = view.findViewById(R.id.nomeCorso);

        // Setup textviews
        String materia = arrayMateriaCorso.get(position).getInsegnamento().getNome() + " " + arrayMateriaCorso.get(position).getInsegnamento().getGruppo();
        textViews[0].setText(materia);
        textViews[1].setText(arrayMateriaCorso.get(position).getInsegnamento().getAnno());
        textViews[2].setText(String.valueOf(arrayMateriaCorso.get(position).getInsegnamento().getCfu()));
        textViews[3].setText(String.valueOf(arrayMateriaCorso.get(position).getInsegnamento().getSemestre()));
        textViews[4].setText(String.valueOf(arrayMateriaCorso.get(position).getNomeCorso()));

        return view;
    }
}
