package it.uniba.di.orariolezioni;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import it.uniba.di.orariolezioni.helper.HttpJsonParser;

/**
 * Fragment relativo alla sezione di richiesta scambi. Tramite questo fragment vengono caricati tutti i dati
 * necessari ad effettuare uno scambio con gli insegnamenti del corso relativo al professore loggato.
 * In caso di più corsi essi saranno entrambi disponibili al professore e risultano comunque indipendenti.
 * Tale fragment utilizza metodi che estendono AsyncTask per recuperare i dati da remoto. Per quanto riguarda
 * i dati personali viene mantenuto esclusivamente l'Id legato a ogni professore per poter identificare su
 * database i campi associati a tale id. Per la selezione delle materie vengono utilizzati i widget spinner
 * mentre per la selzione della data viene utilizzato la classe Calendar
 */
public class RichiestaScambio extends Fragment implements DatePickerDialog.OnDateSetListener {

    SharedPreferences preferenzecondivise;
    public static final String PREFERENZE = "PreferenzePersonali";
    private Spinner spin;
    private Spinner spinInsegnamento;
    private int idCorso;
    private int anno;
    private int idInsegnamento;
    private int idInsegnamentoScambio;
    private String gruppo;
    private ImageView img;
    private int idProfessoreScambio;
    private int[] giorni;
    private Button btn;
    private TextView txtData;
    private TextView testoprof;
    private ImageView img2;
    private Calendar[] enableDays;
    private Calendar[] enableDaysScambio;
    private Calendar dataRichiedente;
    private Calendar dataDestinatario;
    public static final int FLAG_START_DATE = 0;
    public static final int FLAG_END_DATE = 1;
    private String materieDaScambiare[];
    private ArrayList<NodoRichieste> richiedente;
    private ArrayList<NodoRichieste> destinatario;
    private ArrayList<String> orariInizioDestinatario;
    private ArrayList<String> orariFineDestinatario;
    private ArrayList<String> orariInizioRichiedente;
    private ArrayList<String> orariFineRichiedente;
    private static int annoRichiedente;
    private static int meseRichiedente;
    private static int giornoRichiedente;
    private static int annoDestinatario;
    private static int meseDestinatario;
    private static int giornoDestinatario;
    private ArrayList<Nodo> lista;
    private final String GETMATERIESCAMBI = "http://www.orariolezionisms.altervista.org/getMaterieScambi.php";
    private final String GETMATERIECONGIORNO = "http://www.orariolezionisms.altervista.org/getOrarioMateriaConGiorno.php";
    private final String SETSCAMBIO = "http://www.orariolezionisms.altervista.org/postScambio.php";
    private final String GETGIORNISCAMBIATI = "http://www.orariolezionisms.altervista.org/getGiorniScambiati.php";
    public static boolean flag = true;
    private TextView data;

    private class GiorniScambi extends AsyncTask<Void, Void, JSONArray> {

        Map<String, String> mappa = new HashMap<>();

        private GiorniScambi(String... param) {
            mappa.put("IdInsegnamento", param[0]);

        }

        @Override
        protected JSONArray doInBackground(Void... param) {
            try {
                HttpJsonParser parser = new HttpJsonParser();
                JSONArray jObj;
                jObj = parser.makeHttpRequest(GETGIORNISCAMBIATI, "GET", mappa);
                return jObj;
            } catch (Exception e) {
                return null;
            }
        }
    }

    private class Risposta extends AsyncTask<Void, Void, JSONArray> {

        Map<String, String> mappa = new HashMap<>();

        private Risposta(String... param) {
            mappa.put("IdInsegnamento", param[0]);
            mappa.put("IdCorso", param[1]);
            mappa.put("Anno", param[2]);
            mappa.put("Semestre", param[3]);
            mappa.put("Gruppo", param[4]);
        }

        @Override
        protected JSONArray doInBackground(Void... param) {
            try {
                HttpJsonParser parser = new HttpJsonParser();
                JSONArray jObj;
                jObj = parser.makeHttpRequest(GETMATERIESCAMBI, "GET", mappa);
                return jObj;
            } catch (Exception e) {
                return null;
            }
        }
    }

    private class Richiesta extends AsyncTask<Void, Void, JSONArray> {

        Map<String, String> mappa = new HashMap<>();

        private Richiesta(String... param) {
            mappa.put("GiornoRic", param[0].trim());
            mappa.put("MeseRic", param[1].trim());
            mappa.put("AnnoRic", param[2].trim());
            mappa.put("GiornoDest", param[3].trim());
            mappa.put("MeseDest", param[4].trim());
            mappa.put("AnnoDest", param[5].trim());
            mappa.put("IdProfRichiedente", param[6].trim());
            mappa.put("IdProfDestinatario", param[7].trim());
            mappa.put("IdInsegnamentoRichiedente", param[8].trim());
            mappa.put("IdInsegnamentoDestinatario", param[9].trim());
            mappa.put("OraInizioDestinatario", param[10].trim());
            mappa.put("OraFineDestinatario", param[11].trim());
            mappa.put("OraInizioRichiedente", param[12].trim());
            mappa.put("OraFineRichiedente", param[13].trim());
            mappa.put("SettimanaRic", param[14].trim());
            mappa.put("SettimanaDes", param[15].trim());
        }

        @Override
        protected JSONArray doInBackground(Void... param) {
            try {
                HttpJsonParser parser = new HttpJsonParser();
                JSONArray jObj;
                jObj = parser.makeHttpRequest(SETSCAMBIO, "GET", mappa);
                return jObj;
            } catch (Exception e) {
                return null;
            }
        }
    }


    private class MateriaConGiorno extends AsyncTask<Void, Void, JSONArray> {

        Map<String, String> mappa = new HashMap<>();

        private MateriaConGiorno(String... param) {
            mappa.put("IdInsegnamento", param[0].trim());
        }

        @Override
        protected JSONArray doInBackground(Void... param) {
            try {
                HttpJsonParser parser = new HttpJsonParser();
                JSONArray jObj;
                jObj = parser.makeHttpRequest(GETMATERIECONGIORNO, "GET", mappa);
                return jObj;
            } catch (Exception e) {
                return null;
            }
        }
    }

    public class Nodo {
        int IdProfessore;
        String nome;
        String cognome;
        int IdInsegnamento;
        String nomeInsegnamento;
    }

    public class NodoRichieste {
        int giorno;
        ArrayList<String> oraInizio = new ArrayList<>();
        ArrayList<String> oraFine = new ArrayList<>();
    }

    public RichiestaScambio() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.e("callback", "ONCREATE");
        final View rootView = inflater.inflate(R.layout.fragment_richiesta_scambio, container, false);
        spin = rootView.findViewById(R.id.spinner);
        spinInsegnamento = rootView.findViewById(R.id.spinner2);
        img = rootView.findViewById(R.id.imageViewCalendar);
        txtData = rootView.findViewById(R.id.textFunziona);
        testoprof = rootView.findViewById(R.id.textView11);
        img2 = rootView.findViewById(R.id.imageView3);
        // Prendere i dati dal prof
        MainActivity activity = (MainActivity) getActivity();
        final Professore prof = activity.getProfLoggato();
        btn = rootView.findViewById(R.id.button2);
        preferenzecondivise = this.getActivity().getSharedPreferences(PREFERENZE, Context.MODE_PRIVATE);
        // Prendiamo il semestre in cui ci troviamo dalle shared preferences e in base a quello, prendiamo i corsi
        // dello stesso semstre in cui insegna il professore.
        // TODO: rimettere le shared
        //  String semestre = SaveSharedPreferences.getSemestre(rootView.getContext());
        final ArrayList<CorsoLaurea> corso = prof.getCorso();
        ArrayList<Insegnamento> elencoMaterie;

        // Se in uno dei corsi in cui insegna il professore c'è una materia del secondo semestre,
        // allora dobbiamo poter far selezionare al professore il corso

        for (int i = 0; i < corso.size(); i++) {
            // Per ogni corso recuperiamo l'elenco delle materie
            elencoMaterie = corso.get(i).getInsegnamento();
            // Per ogni materia vediamo se ce n'è una al semestre corrente.
            for (int k = 0; k < elencoMaterie.size(); k++) {
                Insegnamento singolaMateria = elencoMaterie.get(k);
                // Se il semestre non è uguale  a quello corrente, rimuoviamo la materia dall'elenco
                // TODO:rimettere semestre SENZA virgolette al posto di 1
                if (!singolaMateria.getSemestre().equals("1")) {
                    elencoMaterie.remove(k);
                }
            }
            // Se elencoMaterie è vuoto, significa che in quel semestre per quel corso di laurea il professore
            // non ha materie, quindi dobbiamo rimuoverlo dalla lista dei corsi di laurea
            if (elencoMaterie.size() == 0) {
                corso.remove(i);
            }
            // Altrimenti assegnamo al corso il nuovo vettore di materie che contengono solo quelle del rispettivo semestre
            else {
                corso.get(i).setInsegnamento(elencoMaterie);
            }
        }

        // A questo punto abbiamo tutti i corsi e tutte le materie che il prof insegna nel semestre corrente
        // Se non ci sono corsi, il professore non può fare scambi
        if (corso.size() == 0) {
            Log.e("NonScambi", "Non puoi fare scambi");
        }
        // Se invece ci sono corsi,dobbiamo dare la possibilità al professore di scegliere LA MATERIA
        // mettiamo in una lista le materie

        ArrayList<Insegnamento> tutteLeMaterie = new ArrayList<>();
        // Inizializziamo la stringa con i nomi dei corsi
        for (int i = 0; i < corso.size(); i++) {
            elencoMaterie = corso.get(i).getInsegnamento();
            for (int k = 0; k < elencoMaterie.size(); k++) {
                Insegnamento singoloInsegnamento = elencoMaterie.get(k);
                tutteLeMaterie.add(singoloInsegnamento);
            }
        }

        // Ora abbiamo una lista di tutte le materie di entrambi i corsi
        final String[] listaMaterie = new String[tutteLeMaterie.size()];
        for (int i = 0; i < tutteLeMaterie.size(); i++) {
            listaMaterie[i] = tutteLeMaterie.get(i).getNome();
        }
        // Creiamo l'adapter per lo spinner che visualizzerà l'elenco delle materie
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(rootView.getContext(), android.R.layout.simple_list_item_1, listaMaterie);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spin.setAdapter(adapter);
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                txtData.setText("");
                String selectedItem = adapterView.getItemAtPosition(position).toString();
                // In base al nome della materia selezionato devo prelevare gli orari dal calendario
                // devo cercare l'id del corso associato alla materia e l'anno
                Log.e("NonScambi", position + "");
                if (selectedItem.equals(listaMaterie[position])) {
                    for (int i = 0; i < corso.size(); i++) {
                        ArrayList<Insegnamento> elenco = corso.get(i).getInsegnamento();
                        for (int k = 0; k < elenco.size(); k++) {
                            if (elenco.get(k).getNome().equals(listaMaterie[position])) {
                                // Recuperiamo il corso di laurea e l'anno
                                idCorso = corso.get(i).getIdCorso();
                                anno = Integer.parseInt(elenco.get(k).getAnno());
                                idInsegnamento = (elenco.get(k).getId());
                                if (elenco.get(k).getGruppo().isEmpty()) {
                                    gruppo = "\"\"";
                                } else {
                                    gruppo = "\"" + elenco.get(k).getGruppo() + "\"";
                                }
                            }
                        }
                    }

                    // Lanciamo la async per vedere quali giorni della settimana fa lezione il professore richiedente
                    MateriaConGiorno risposta = new MateriaConGiorno(idInsegnamento + "");
                    JSONArray calendario = null;

                    try {
                        calendario = risposta.execute().get();
                        richiedente = new ArrayList<>();
                        getGiorni(calendario, richiedente);
                        ArrayList<Calendar> weekends = new ArrayList<>();
                        int weeks = 52;
                        for (int f = 0; f < giorni.length; f++) {
                            switch (giorni[f]) {
                                case 1:
                                    for (int i = 0; i < (weeks * 7); i = i + 7) {
                                        Calendar lunedì = Calendar.getInstance();
                                        lunedì.add(Calendar.DAY_OF_YEAR, (Calendar.MONDAY - lunedì.get(Calendar.DAY_OF_WEEK) + i));
                                        weekends.add(lunedì);
                                    }
                                    break;

                                case 2:
                                    for (int i = 0; i < (weeks * 7); i = i + 7) {
                                        Calendar martedi = Calendar.getInstance();
                                        martedi.add(Calendar.DAY_OF_YEAR, (Calendar.TUESDAY - martedi.get(Calendar.DAY_OF_WEEK) + i));
                                        weekends.add(martedi);
                                    }
                                    break;

                                case 3:
                                    for (int i = 0; i < (weeks * 7); i = i + 7) {
                                        Calendar mercoledi = Calendar.getInstance();
                                        mercoledi.add(Calendar.DAY_OF_YEAR, (Calendar.WEDNESDAY - mercoledi.get(Calendar.DAY_OF_WEEK) + i));
                                        weekends.add(mercoledi);
                                    }
                                    break;

                                case 4:
                                    for (int i = 0; i < (weeks * 7); i = i + 7) {
                                        Calendar giovedi = Calendar.getInstance();
                                        giovedi.add(Calendar.DAY_OF_YEAR, (Calendar.THURSDAY - giovedi.get(Calendar.DAY_OF_WEEK) + i));
                                        weekends.add(giovedi);
                                    }
                                    break;

                                case 5:
                                    for (int i = 0; i < (weeks * 7); i = i + 7) {
                                        Calendar venerdi = Calendar.getInstance();
                                        venerdi.add(Calendar.DAY_OF_YEAR, (Calendar.FRIDAY - venerdi.get(Calendar.DAY_OF_WEEK) + i));
                                        weekends.add(venerdi);
                                    }
                                    break;
                            }
                        }

                        enableDays = weekends.toArray(new Calendar[weekends.size()]);
                        DatePickerDialog dp = new DatePickerDialog();
                        //A questo punto, dopo aver abilitato i giorni,lanciamo una async che preleva i giorni
                        //in cui sono stati effettuati degli scambi,per disabilitarli.
                        GiorniScambi giorniScambi = new GiorniScambi(idInsegnamento + "");
                        JSONArray objArray = null;
                        try {
                            objArray = giorniScambi.execute().get();
                            enableDays = parseScambioGiorno(objArray, enableDays);
                            dp.setSelectableDays(enableDays);
                        } catch (ExecutionException e) {

                        }


                        Calendar cal = null;
                        // Rendiamo disponibili i giorni selezionabili in base alle lezioni del professore
                        for (int i = 0; i < enableDays.length; i++) {
                            if (enableDays[i].get(Calendar.DAY_OF_MONTH) >= Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
                                    && enableDays[i].get(Calendar.MONTH) == Calendar.getInstance().get(Calendar.MONTH)) {
                                cal = enableDays[i];
                                annoRichiedente = cal.get(Calendar.YEAR);
                                meseRichiedente = cal.get(Calendar.MONTH);
                                giornoRichiedente = cal.get(Calendar.DAY_OF_MONTH);
                                Log.e("day", enableDays[i].get(Calendar.DAY_OF_MONTH) + "");
                                break;
                            } else if(enableDays[i].after(Calendar.getInstance())){
                                annoRichiedente = enableDays[i].get(Calendar.YEAR);
                                meseRichiedente = enableDays[i].get(Calendar.MONTH);
                                giornoRichiedente = enableDays[i].get(Calendar.DAY_OF_MONTH);
                                break;
                            }
                        }

                        // A questo punto facciamo scegliere al professore la data in cui effettuare lo scambio
                        img.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Calendar now = Calendar.getInstance();

                                DatePickerDialog dpd = DatePickerDialog.newInstance(RichiestaScambio.this,
                                        now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
                                dpd.setSelectableDays(enableDays);
                                dpd.setMinDate(Calendar.getInstance());
                                Calendar cal = null;
                                // Rendiamo i giorni disponibili selezionabili in base alle ora del professore
                                for (int i = 0; i < enableDays.length; i++) {
                                    if (enableDays[i].get(Calendar.DAY_OF_MONTH) >= Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
                                            && enableDays[i].get(Calendar.MONTH) == Calendar.getInstance().get(Calendar.MONTH)) {
                                        cal = enableDays[i];
                                        annoRichiedente = cal.get(Calendar.YEAR);
                                        meseRichiedente = cal.get(Calendar.MONTH);
                                        giornoRichiedente = cal.get(Calendar.DAY_OF_MONTH);
                                        Log.e("day", enableDays[i].get(Calendar.DAY_OF_MONTH) + "");
                                        break;
                                    } else if(enableDays[i].after(Calendar.getInstance())){
                                        annoRichiedente = enableDays[i].get(Calendar.YEAR);
                                        meseRichiedente = enableDays[i].get(Calendar.MONTH);
                                        giornoRichiedente = enableDays[i].get(Calendar.DAY_OF_MONTH);
                                        break;
                                    }
                                }

                                dpd.initialize(new DatePickerDialog.OnDateSetListener() {
                                    @Override
                                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                        int meseCorretto = monthOfYear + 1;
                                        txtData.setText(dayOfMonth + "/" + meseCorretto);
                                        dataRichiedente = Calendar.getInstance();
                                        dataRichiedente.set(year, monthOfYear, dayOfMonth);
                                        int dayOfWeek;
                                        switch (dataRichiedente.get(Calendar.DAY_OF_WEEK)) {
                                            case 2:
                                                dayOfWeek = 1;
                                                break;
                                            case 3:
                                                dayOfWeek = 2;
                                                break;
                                            case 4:
                                                dayOfWeek = 3;
                                                break;
                                            case 5:
                                                dayOfWeek = 4;
                                                break;
                                            case 6:
                                                dayOfWeek = 5;
                                                break;
                                            default:
                                                dayOfWeek = 6;
                                                break;
                                        }

                                        txtData.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                        for (int k = 0; k < richiedente.size(); k++) {
                                            if (richiedente.get(k).giorno == dayOfWeek) {
                                                orariInizioRichiedente = richiedente.get(k).oraInizio;
                                                orariFineRichiedente = richiedente.get(k).oraFine;
                                                annoRichiedente = year;
                                                meseRichiedente = monthOfYear;
                                                giornoRichiedente = dayOfMonth;
                                                Log.e("result", "RICHIEDENTE" + richiedente.get(k).giorno + "" + orariInizioRichiedente + "" + orariFineRichiedente);
                                            }
                                        }
                                    }
                                }, annoRichiedente, meseRichiedente, giornoRichiedente);
                                dpd.setAccentColor(getResources().getColor(R.color.colorPrimaryDark));
                                dpd.show(getActivity().getFragmentManager(), "Date picker");
                            }
                        });
                        int meseCorretto = meseRichiedente + 1;
                        txtData.setText(giornoRichiedente + "/" + meseCorretto);
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    //A questo punto possiamo fare la richiesta al server per recuperare il calendario associato alla materia selezionata

                    // TODO: METTERE IL SEMESTRE
                    Risposta risp = new Risposta(idInsegnamento + "", idCorso + "", anno + "", "1", gruppo);
                    JSONArray obj = null;

                    try {
                        obj = risp.execute().get();
                        getOrario(obj);
                        Log.e("NonScambi", lista.get(0).nomeInsegnamento);
                        materieDaScambiare = new String[lista.size()];
                        for (int k = 0; k < lista.size(); k++) {
                            materieDaScambiare[k] = lista.get(k).nomeInsegnamento;
                            Log.e("NonScambio", materieDaScambiare[k]);
                        }
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    // Creiamo l'adapter per lo spinner che visualizzerà l'elenco delle materie con cui effettuare lo scambio
                    ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(rootView.getContext(), android.R.layout.simple_list_item_1, materieDaScambiare);
                    adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinInsegnamento.setAdapter(adapter2);

                    spinInsegnamento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                            String selectedItem = adapterView.getItemAtPosition(position).toString();
                            testoprof.setText("");
                            // Prendiamo l'id del professore che ha selezionato
                            if (selectedItem.equals(materieDaScambiare[position])) {
                                for (int k = 0; k < lista.size(); k++) {
                                    if (materieDaScambiare[position].equals(lista.get(k).nomeInsegnamento)) {
                                        // Prendiamo l'id del professore con cui fare lo scambio
                                        idProfessoreScambio = lista.get(k).IdProfessore;
                                        idInsegnamentoScambio = lista.get(k).IdInsegnamento;
                                    }
                                }

                                // Facciamo la richiesta al server per prendere l'orario della materia
                                MateriaConGiorno risposta = new MateriaConGiorno(idInsegnamentoScambio + "");
                                JSONArray calendario = null;

                                try {
                                    calendario = risposta.execute().get();
                                    destinatario = new ArrayList<>();
                                    getGiorni(calendario, destinatario);
                                    ArrayList<Calendar> weekends = new ArrayList<>();
                                    int weeks = 52;

                                    for (int f = 0; f < giorni.length; f++) {
                                        switch (giorni[f]) {
                                            case 1:
                                                for (int i = 0; i < (weeks * 7); i = i + 7) {
                                                    Calendar lunedì = Calendar.getInstance();
                                                    lunedì.add(Calendar.DAY_OF_YEAR, (Calendar.MONDAY - lunedì.get(Calendar.DAY_OF_WEEK) + i));
                                                    weekends.add(lunedì);
                                                }
                                                break;

                                            case 2:
                                                for (int i = 0; i < (weeks * 7); i = i + 7) {
                                                    Calendar martedi = Calendar.getInstance();
                                                    martedi.add(Calendar.DAY_OF_YEAR, (Calendar.TUESDAY - martedi.get(Calendar.DAY_OF_WEEK) + i));
                                                    weekends.add(martedi);
                                                }
                                                break;

                                            case 3:
                                                for (int i = 0; i < (weeks * 7); i = i + 7) {
                                                    Calendar mercoledi = Calendar.getInstance();
                                                    mercoledi.add(Calendar.DAY_OF_YEAR, (Calendar.WEDNESDAY - mercoledi.get(Calendar.DAY_OF_WEEK) + i));
                                                    weekends.add(mercoledi);
                                                }
                                                break;

                                            case 4:
                                                for (int i = 0; i < (weeks * 7); i = i + 7) {
                                                    Calendar giovedi = Calendar.getInstance();
                                                    giovedi.add(Calendar.DAY_OF_YEAR, (Calendar.THURSDAY - giovedi.get(Calendar.DAY_OF_WEEK) + i));
                                                    weekends.add(giovedi);
                                                }
                                                break;

                                            case 5:
                                                for (int i = 0; i < (weeks * 7); i = i + 7) {
                                                    Calendar venerdi = Calendar.getInstance();
                                                    venerdi.add(Calendar.DAY_OF_YEAR, (Calendar.FRIDAY - venerdi.get(Calendar.DAY_OF_WEEK) + i));
                                                    weekends.add(venerdi);
                                                }
                                                break;
                                        }
                                    }

                                    enableDaysScambio = weekends.toArray(new Calendar[weekends.size()]);
                                    DatePickerDialog pd = new DatePickerDialog();
                                    GiorniScambi giorniScambi = new GiorniScambi(idInsegnamentoScambio + "");
                                    JSONArray objArray = null;
                                    try {
                                        objArray = giorniScambi.execute().get();
                                        enableDaysScambio= parseScambioGiorno(objArray, enableDaysScambio);
                                        pd.setSelectableDays(enableDaysScambio);
                                    } catch (ExecutionException e) {

                                    }
                                    Calendar cal = null;
                                    for (int i = 0; i < enableDaysScambio.length; i++) {
                                        if (enableDaysScambio[i].get(Calendar.DAY_OF_MONTH) >= Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
                                                && enableDaysScambio[i].get(Calendar.MONTH) == Calendar.getInstance().get(Calendar.MONTH)) {
                                            cal = enableDaysScambio[i];
                                            annoDestinatario = cal.get(Calendar.YEAR);
                                            meseDestinatario = cal.get(Calendar.MONTH);
                                            giornoDestinatario = cal.get(Calendar.DAY_OF_MONTH);
                                            Log.e("day", enableDaysScambio[i].get(Calendar.DAY_OF_MONTH) + "");
                                            break;
                                        } else if(enableDaysScambio[i].after(Calendar.getInstance())){
                                            annoDestinatario = enableDaysScambio[i].get(Calendar.YEAR);
                                            meseDestinatario = enableDaysScambio[i].get(Calendar.MONTH);
                                            giornoDestinatario = enableDaysScambio[i].get(Calendar.DAY_OF_MONTH);
                                            break;
                                        }
                                    }

                                    // A questo punto facciamo scegliere al professore la data in cui effettuare lo scambio
                                    img2.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            Calendar now = Calendar.getInstance();

                                            DatePickerDialog dpd = DatePickerDialog.newInstance(RichiestaScambio.this,
                                                    now.get(Calendar.YEAR),
                                                    now.get(Calendar.MONTH),
                                                    now.get(Calendar.DAY_OF_MONTH));
                                            dpd.setMinDate(Calendar.getInstance());
                                            dpd.setSelectableDays(enableDaysScambio);
                                            Calendar cal = null;

                                            for (int i = 0; i < enableDaysScambio.length; i++) {
                                                if (enableDaysScambio[i].get(Calendar.DAY_OF_MONTH) >= Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
                                                        && enableDaysScambio[i].get(Calendar.MONTH) == Calendar.getInstance().get(Calendar.MONTH)) {
                                                    cal = enableDaysScambio[i];
                                                    annoDestinatario = cal.get(Calendar.YEAR);
                                                    meseDestinatario = cal.get(Calendar.MONTH);
                                                    giornoDestinatario = cal.get(Calendar.DAY_OF_MONTH);
                                                    Log.e("day", enableDaysScambio[i].get(Calendar.DAY_OF_MONTH) + "");
                                                    break;
                                                }else if(enableDaysScambio[i].after(Calendar.getInstance())){
                                                    annoDestinatario = enableDaysScambio[i].get(Calendar.YEAR);
                                                    meseDestinatario = enableDaysScambio[i].get(Calendar.MONTH);
                                                    giornoDestinatario = enableDaysScambio[i].get(Calendar.DAY_OF_MONTH);
                                                    break;
                                                }
                                            }
                                            dpd.initialize(new DatePickerDialog.OnDateSetListener() {
                                                @Override
                                                public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                                    int meseCorretto = monthOfYear + 1;
                                                    testoprof.setText(dayOfMonth + "/" + meseCorretto);
                                                    dataDestinatario = Calendar.getInstance();
                                                    dataDestinatario.set(year, monthOfYear, dayOfMonth);
                                                    int dayOfWeek;
                                                    switch (dataDestinatario.get(Calendar.DAY_OF_WEEK)) {
                                                        case 2:
                                                            dayOfWeek = 1;
                                                            break;
                                                        case 3:
                                                            dayOfWeek = 2;
                                                            break;
                                                        case 4:
                                                            dayOfWeek = 3;
                                                            break;
                                                        case 5:
                                                            dayOfWeek = 4;
                                                            break;
                                                        case 6:
                                                            dayOfWeek = 5;
                                                            break;
                                                        default:
                                                            dayOfWeek = 6;
                                                            break;
                                                    }
                                                    testoprof.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                                                    for (int k = 0; k < destinatario.size(); k++) {
                                                        if (destinatario.get(k).giorno == dayOfWeek) {
                                                            orariInizioDestinatario = destinatario.get(k).oraInizio;
                                                            orariFineDestinatario = destinatario.get(k).oraFine;
                                                            Log.e("result", "DESTINATARIO" + destinatario.get(k).giorno + "" + orariInizioDestinatario + "" + orariFineDestinatario);
                                                            annoDestinatario = year;
                                                            meseDestinatario = monthOfYear;
                                                            giornoDestinatario = dayOfMonth;
                                                        }
                                                    }
                                                }
                                            }, annoDestinatario, meseDestinatario, giornoDestinatario);
                                            dpd.setAccentColor(getResources().getColor(R.color.colorPrimaryDark));
                                            dpd.show(getActivity().getFragmentManager(), "Date picker");
                                        }
                                    });
                                    int meseCorretto = meseDestinatario + 1;
                                    testoprof.setText(giornoDestinatario + "/" + meseCorretto);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                } catch (ExecutionException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String idProfessore = preferenzecondivise.getString("IdProfessore", String.valueOf(0));
                if (dataDestinatario == null || dataRichiedente == null) {
                    Toast.makeText(getContext(), R.string.RichiestaNonEffettuata, Toast.LENGTH_LONG).show();
                } else {
                    int weekDest = dataDestinatario.get(Calendar.WEEK_OF_YEAR);
                    int weekRic = dataRichiedente.get(Calendar.WEEK_OF_YEAR);
                    for (int k = 0; k < orariInizioDestinatario.size(); k++) {
                        Richiesta risp = new Richiesta(giornoRichiedente + "", meseRichiedente + "", annoRichiedente + "",
                                giornoDestinatario + "", meseDestinatario + "", annoDestinatario + "",
                                idProfessore, idProfessoreScambio + "", idInsegnamento + "", idInsegnamentoScambio + "",
                                orariInizioDestinatario.get(k), orariFineDestinatario.get(k),
                                orariInizioRichiedente.get(k), orariFineRichiedente.get(k), weekRic + "", weekDest + "");
                        risp.execute();
                    }

                    try {
                        Toast.makeText(getContext(), R.string.RichiestaEffettuata, Toast.LENGTH_LONG).show();
                        new sendnotify(String.valueOf(idProfessoreScambio),
                                getContext().getString(R.string.NotifyRequest),
                                getContext().getString(R.string.msgNotifyRequest)).execute().get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }

                    FragmentCronologia.flag = false;
                    FragmentScambio.flag = false;
                }


            }
        });
        return rootView;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
    }

    public void getOrario(JSONArray object) {
        ArrayList<JSONObject> elenco = new ArrayList<>();
        for (int i = 0; i < object.length(); i++) {
            try {
                elenco.add((JSONObject) object.get(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //NELL'ELENCO CI SONO TUTTI I JSON (LE RIGHE DEL PHP)
        JSONObject ora;
        lista = new ArrayList<>();
        for (int i = 0; i < elenco.size(); i++) {
            try {
                Nodo valori = new Nodo();
                ora = elenco.get(i);
                valori.IdProfessore = Integer.parseInt(ora.getString("IdProfessore"));
                valori.IdInsegnamento = Integer.parseInt(ora.getString("IdInsegnamento"));
                valori.cognome = ora.getString("CognomeProfessore");
                valori.nome = ora.getString("NomeProfessore");
                valori.nomeInsegnamento = ora.getString("NomeInsegnamento");
                lista.add(valori);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {
            onResume();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }
        if(!flag){
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
            flag = true;
        }
        int meseCorretto1 = meseRichiedente + 1;
        int meseCorretto2 = meseDestinatario + 1;
        txtData.setText(giornoRichiedente + "/" + meseCorretto1);
        testoprof.setText(giornoDestinatario + "/" + meseCorretto2);
    }

    public void getGiorni(JSONArray obj, ArrayList<NodoRichieste> elencoOrari) {
        ArrayList<JSONObject> elenco = new ArrayList<>();
        for (int i = 0; i < obj.length(); i++) {
            try {
                elenco.add((JSONObject) obj.get(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        JSONObject oggetto;
        // Nell'elenco ci sono i JSONObject
        ArrayList<Integer> interi = new ArrayList<>();
        NodoRichieste nodo = new NodoRichieste();
        for (int i = 0; i < elenco.size(); i++) {
            try {
                oggetto = elenco.get(i);
                if (interi.isEmpty()) {
                    interi.add(Integer.parseInt(oggetto.getString("Giorno")));
                } else if (!interi.contains(Integer.parseInt(oggetto.getString("Giorno")))) {
                    interi.add(Integer.parseInt(oggetto.getString("Giorno")));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        giorni = new int[interi.size()];

        for (int k = 0; k < interi.size(); k++) {
            giorni[k] = interi.get(k);
            elencoOrari.add(new NodoRichieste());
            elencoOrari.get(k).giorno = giorni[k];
        }

        for (int i = 0; i < elenco.size(); i++) {
            try {
                oggetto = elenco.get(i);
                for (int k = 0; k < elencoOrari.size(); k++) {
                    if (elencoOrari.get(k).giorno == (Integer.parseInt(oggetto.getString("Giorno")))) {
                        elencoOrari.get(k).oraInizio.add(oggetto.getString("OraI"));
                        elencoOrari.get(k).oraFine.add(oggetto.getString("OraF"));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Classe per l'invio di notifiche al destinatario dello scambio
     */
    public static class sendnotify extends AsyncTask<Void, Void, JSONArray> {

        private final String CONFIRM = "http://www.orariolezionisms.altervista.org/postnot.php";
        Map<String, String> mappa = new HashMap<>();

        private sendnotify(String... param) {
            mappa.put("IdProfessore", param[0].trim());
            mappa.put("Titolo", param[1]);
            mappa.put("Messaggio", param[2]);
        }

        @Override
        protected JSONArray doInBackground(Void... params) {
            try {

                HttpJsonParser parser = new HttpJsonParser();
                JSONArray jObj;
                jObj = parser.makeHttpRequest(CONFIRM, "GET", mappa);
                return jObj;

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public Calendar[] parseScambioGiorno(JSONArray objArray, Calendar[] elencoDate) {
        Calendar today = Calendar.getInstance();

        ArrayList<Calendar> vettoreData = new ArrayList<>();
        ArrayList<JSONObject> elencoScambi = new ArrayList<>();

        for (int i = 0; i < objArray.length(); i++) {
            try {
                elencoScambi.add((JSONObject) objArray.get(i));
                Calendar day = Calendar.getInstance();
                day.set(elencoScambi.get(i).getInt("Anno"), elencoScambi.get(i).getInt("Mese"), elencoScambi.get(i).getInt("Giorno"));
                //Se la data dello scaambio non è più piccola rispetto a quella di oggi, la aggiungiamo nel vettore di appoggio
                if (!day.before(today)) {
                    vettoreData.add(day);

                }

            } catch (JSONException e) {

            }

        }

        //vettoreData contiene tutte le date degli scambi postOdierni
        //A questo punto,per ogni elemento di elencoDate, se c'è una data che matcha, quella data deve essere rimossa
        //perchè non deve essere abilitata visto che c'è stato uno scambio
        ArrayList<Calendar> elenco = new ArrayList<>();
        for (int k = 0; k < elencoDate.length; k++) {
            elenco.add(elencoDate[k]);

        }


        //Rimuoviamo dall'elenco delle date disponibili le date in cui sono effettuati degli scambi
        for (int m = 0; m < elenco.size(); m++) {
            Log.e("url", "elenconuovo" + elenco.get(m).get(Calendar.DAY_OF_MONTH) + "/" + elenco.get(m).get(Calendar.MONTH));
            for (int f = 0; f < vettoreData.size(); f++) {
                if (elenco.get(m).get(Calendar.DAY_OF_MONTH) == vettoreData.get(f).get(Calendar.DAY_OF_MONTH)
                        && elenco.get(m).get(Calendar.MONTH) == vettoreData.get(f).get(Calendar.MONTH)
                        && elenco.get(m).get(Calendar.YEAR) == vettoreData.get(f).get(Calendar.YEAR)) {
                    Log.e("url", "rimuovi" + elenco.get(m).get(Calendar.DAY_OF_MONTH) + "/" + elenco.get(m).get(Calendar.MONTH));
                    elenco.remove(m);
                }
            }
        }
        elencoDate = new Calendar[elenco.size()];

        for (int i = 0; i < elenco.size(); i++) {
            elencoDate[i] = elenco.get(i);

        }

        return elencoDate;
    }
}
