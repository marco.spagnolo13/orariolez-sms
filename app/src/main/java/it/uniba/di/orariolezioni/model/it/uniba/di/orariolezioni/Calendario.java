package it.uniba.di.orariolezioni;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Adapter utilizzato per la stampa del calendario. Ogni textview relative alla gridview è indipendente e controllata
 * singolarmente all'interno di una switch. All'interno di ogni cella della gridview vi è stampato il nome della lezione
 * di quel giorno. Il calendario si aggiorna a runtime in base alle notifiche ricevute o agli scambi confermati.
 */
public class Calendario extends BaseAdapter {

    private final Context mContext;
    private final int DIM = 66;
    private TextView txt;
    private JSONArray object;
    private JSONArray ricevuto;
    private ArrayList<Orario> lista;
    GridView grid;
    private ArrayList<Orario> elencoOreR;
    private ArrayList<Orario> elencoOreD;
    private int currentSettimana;

    private ArrayList<Orario> calendarioConScambi;
    ArrayList<Orario> scambio;

    public class Orario {

        int idOraInizio;
        String oraInizio;
        int idOraFine;
        String oraFine;
        String aula;
        int giorno;
        Insegnamento insegnamento;
        Professore professore;
        int idCorsoLaurea;
        String nomeCorso;

        public Orario(int idOraInizio, String oraInizio, int idOraFine, String oraFine,
                      String aula, int giorno, Insegnamento insegnamento, Professore professore,
                      int idCorsoLaurea, String nomeCorso) {
            this.idOraInizio = idOraInizio;
            this.oraInizio = oraInizio;
            this.idOraFine = idOraFine;
            this.oraFine = oraFine;
            this.aula = aula;
            this.giorno = giorno;
            this.insegnamento = insegnamento;
            this.professore = professore;
            this.idCorsoLaurea = idCorsoLaurea;
            this.nomeCorso = nomeCorso;
        }

        public Orario(String oraInizio, String oraFine,
                      int giorno, Insegnamento insegnamento, Professore professore) {
            this.oraInizio = oraInizio;
            this.oraFine = oraFine;
            this.giorno = giorno;
            this.insegnamento = insegnamento;
            this.professore = professore;
        }

        @Override
        public boolean equals(Object ora2) {
            super.equals(ora2);
            Orario nuovaOra = (Orario) ora2;
            if (this.giorno == nuovaOra.giorno && this.oraInizio.equals(nuovaOra.oraInizio) && this.oraFine.equals(nuovaOra.oraFine)) {
                return true;
            } else {
                return false;
            }
        }
    }


    public Calendario(Context mContext, JSONArray object, GridView grid, JSONArray ricevuto, int currentSettimana) {
        this.mContext = mContext;
        this.ricevuto = ricevuto;
        this.grid = grid;
        this.object = object;
        this.currentSettimana = currentSettimana;
    }

    @Override
    public int getCount() {
        return DIM;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View newView = null;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        newView = inflater.inflate(R.layout.calendar_cell, parent, false);

        txt = newView.findViewById(R.id.textcal);
        txt.setBackgroundColor(Color.parseColor("#ffffff"));
        ListView.LayoutParams textParam = new ListView.LayoutParams
                (180, 230, 0);
        txt.setLayoutParams(textParam);

        // Setup orari nella prima colonna
        switch (position) {
            case 0:
                txt.setText("8:30");
                txt.setTextSize(15);
                txt.setGravity(Gravity.RIGHT);
                txt.setPadding(0, 70, 0, 70);
                break;
            case 6:
                txt.setText("9:30");
                txt.setTextSize(15);
                txt.setGravity(Gravity.RIGHT);
                txt.setPadding(0, 70, 0, 70);
                break;
            case 12:
                txt.setText("10:30");
                setText();
                break;
            case 18:
                txt.setText("11:30");
                setText();
                break;
            case 24:
                txt.setText("12:30");
                setText();
                break;
            case 30:
                txt.setText("13:30");
                setText();
                break;
            case 36:
                txt.setText("14:30");
                setText();
                break;
            case 42:
                txt.setText("15:30");
                setText();
                break;
            case 48:
                txt.setText("16:30");
                setText();
                break;
            case 54:
                txt.setText("17:30");
                setText();
                break;
            case 60:
                txt.setText("18:30");
                setText();
                break;
            default:
                break;
        }

        // Fetch JSONArray in array di obj
        ArrayList<JSONObject> scambi = new ArrayList<>();
        for (int k = 0; k < ricevuto.length(); k++) {
            try {
                scambi.add((JSONObject) ricevuto.get(k));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (scambi.size() == 1) {
            //non ci sono scambi
            getOrario();
            setUpCalendario(txt, position, lista);
        } else {
            //dobbiamo effettuare una combinazione tra l'orario e gli scambi
            getOrario();
            getScambi(scambi);
            joinCalendario();
            setUpCalendario(txt, position, lista);
        }
        return txt;
    }

    // Setup generico testi
    public void setText() {
        txt.setTextSize(13);
        txt.setGravity(Gravity.RIGHT);
        txt.setPadding(0, 70, 0, 70);
    }

    /**
     * Metodo per decodificare l'Array di JsonObject in ArrayList
     */
    public void getOrario() {

        ArrayList<JSONObject> elenco = new ArrayList<>();
        for (int i = 0; i < object.length(); i++) {
            try {
                elenco.add((JSONObject) object.get(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //NELL'ELENCO CI SONO TUTTI I JSON (LE RIGHE DEL PHP)
        JSONObject ora;
        Orario orario;
        lista = new ArrayList<>();
        Insegnamento insegnamento;
        Professore professore;

        for (int i = 0; i < elenco.size(); i++) {
            try {
                ora = elenco.get(i);
                insegnamento = new Insegnamento(Integer.parseInt(ora.getString("IdInsegnamento")), ora.getString("NomeInsegnamento"), ora.getString("Colore"));
                professore = new Professore(Integer.parseInt(ora.getString("IdProfessore")), ora.getString("NomeProfessore"), ora.getString("CognomeProfessore"));
                orario = new Orario(Integer.parseInt(ora.getString("OraInizio")), ora.getString("OraI"), Integer.parseInt(ora.getString("OraFine")), ora.getString("OraF"),
                        ora.getString("Aula"), Integer.parseInt(ora.getString("Giorno")), insegnamento, professore, Integer.parseInt(ora.getString("IdCorsoLaurea")),
                        ora.getString("NomeCorsoLaurea"));
                lista.add(orario);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Metodo per la selezione degli scambi della settimana considerata
     *
     * @param elenco ArrayList popolata degli scambi
     */
    public void getScambi(ArrayList<JSONObject> elenco) {

        // Elenco popolato dalle righe del php
        JSONObject oggetto;
        Insegnamento insegnamentoD;
        Insegnamento insegnamentoR;
        Professore professoreD;
        Professore professoreR;
        Orario orarioD;
        Orario orarioR;

        scambio = new ArrayList<>();

        for (int i = 0; i < elenco.size(); i++) {
            try {
                oggetto = elenco.get(i);
                Log.e("url", oggetto.getString("Destinatario"));

                insegnamentoD = new Insegnamento(Integer.parseInt(oggetto.getString("InsegnamentoDestinatario")), oggetto.getString("NomeInsegnamentoDest"), oggetto.getString("ColoreDest"));
                Log.e("url", oggetto.getString("NomeInsegnamentoDest"));
                professoreD = new Professore(Integer.parseInt(oggetto.getString("Destinatario")), oggetto.getString("ProfDestNome"), oggetto.getString("ProfDestCognome"));
                insegnamentoR = new Insegnamento(Integer.parseInt(oggetto.getString("InsegnamentoRichiedente")), oggetto.getString("NomeInsegnamentoRic"), oggetto.getString("ColoreRic"));
                professoreR = new Professore(Integer.parseInt(oggetto.getString("Richiedente")), oggetto.getString("ProfRicNome"), oggetto.getString("ProfRicCognome"));
                int meseD = Integer.parseInt(oggetto.getString("MeseDest"));
                int annoD = Integer.parseInt(oggetto.getString("AnnoDest"));
                int giornoD = Integer.parseInt(oggetto.getString("GiornoDest"));
                int meseR = oggetto.getInt("MeseRic");
                int annoR = Integer.parseInt(oggetto.getString("AnnoRic"));
                int giornoR = oggetto.getInt("GiornoRic");
                Log.e("url", giornoD + "/" + meseD + "/" + annoD + "");
                Calendar dateDest = Calendar.getInstance();
                dateDest.set(annoR, meseR - 1, giornoR);
                int giornoSettimanaR = 0;
                if (dateDest.get(Calendar.WEEK_OF_YEAR) == currentSettimana) {
                    switch (dateDest.get(Calendar.DAY_OF_WEEK)) {
                        case 2:
                            giornoSettimanaR = 1;
                            break;
                        case 3:
                            giornoSettimanaR = 2;
                            break;
                        case 4:
                            giornoSettimanaR = 3;
                            break;
                        case 5:
                            giornoSettimanaR = 4;
                            break;
                        case 6:
                            giornoSettimanaR = 5;
                            break;
                        default:
                            giornoSettimanaR = 6;
                            break;
                    }
                    orarioD = new Orario(oggetto.getString("InizioRic"), oggetto.getString("FineRic"),
                            giornoSettimanaR, insegnamentoD, professoreD);

                    Log.e("sd", orarioD.giorno + orarioD.insegnamento.getNome() + orarioD.oraInizio + orarioD.oraFine);
                    scambio.add(orarioD);
                }

                // Selezioniamo solo gli scambi presenti nella stessa settimana
                Calendar dateRic = Calendar.getInstance();
                dateRic.set(annoD, meseD - 1, giornoD);
                if (dateRic.get(Calendar.WEEK_OF_YEAR) == currentSettimana) {
                    int giornoSettimanaD = 0;
                    switch (dateRic.get(Calendar.DAY_OF_WEEK)) {
                        case 2:
                            giornoSettimanaD = 1;
                            break;
                        case 3:
                            giornoSettimanaD = 2;
                            break;
                        case 4:
                            giornoSettimanaD = 3;
                            break;
                        case 5:
                            giornoSettimanaD = 4;
                            break;
                        case 6:
                            giornoSettimanaD = 5;
                            break;
                        default:
                            giornoSettimanaD = 6;
                            break;
                    }
                    orarioR = new Orario(oggetto.getString("InizioDest"), oggetto.getString("FineDest"),
                            giornoSettimanaD, insegnamentoR, professoreR);

                    Log.e("sd", orarioR.giorno + orarioR.insegnamento.getNome() + orarioR.oraInizio + orarioR.oraFine);
                    scambio.add(orarioR);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Metodo per il popolamento del calendario
     *
     * @param txt      textview di riferimento da popolare
     * @param position posizione nell'elenco dell'elemento
     * @param lista    lista dell'orario definitivo
     */
    public void setUpCalendario(TextView txt, int position, ArrayList<Orario> lista) {

        Orario ora;
        TextView textView;

        for (int i = 0; i < lista.size(); i++) {
            ora = lista.get(i);
            //Log.e("elenco", ora.insegnamento.getNome() + ora.insegnamento.getColore() + "" + ora.oraInizio + "" + ora.giorno);
            switch (ora.giorno) {

                case 1:
                    switch (ora.oraInizio) {

                        case "8:30":
                            if (position == 1) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "9:30":
                            if (position == 7) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "10:30":
                            if (position == 13) {
                                setUpTextViews(txt, ora);


                            }
                            break;

                        case "11:30":
                            if (position == 19) {
                                setUpTextViews(txt, ora);


                            }
                            break;

                        case "12:30":
                            if (position == 25) {
                                setUpTextViews(txt, ora);


                            }
                            break;

                        case "13:30":
                            if (position == 31) {
                                setUpTextViews(txt, ora);
                               
                            }
                            break;

                        case "14:30":
                            if (position == 37) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "15:30":
                            if (position == 43) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "16:30":
                            if (position == 49) {
                                setUpTextViews(txt, ora);
                            }
                            break;

                        case "17:30":
                            if (position == 55) {
                                setUpTextViews(txt, ora);
                            }
                            break;

                        case "18:30":
                            if (position == 61) {
                                setUpTextViews(txt, ora);
                            }
                            break;
                    }
                    break;

                case 2:

                    switch (ora.oraInizio) {
                        case "8:30":
                            if (position == 2) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "9:30":
                            if (position == 8) {
                                setUpTextViews(txt, ora);
                            }
                            break;

                        case "10:30":
                            if (position == 14) {
                                setUpTextViews(txt, ora);
                            }
                            break;

                        case "11:30":
                            if (position == 20) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "12:30":
                            if (position == 26) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "13:30":
                            if (position == 32) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "14:30":
                            if (position == 38) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "15:30":
                            if (position == 44) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "16:30":
                            if (position == 50) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "17:30":
                            if (position == 56) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "18:30":
                            if (position == 62) {
                                setUpTextViews(txt, ora);

                            }
                            break;
                    }
                    break;

                case 3:

                    switch (ora.oraInizio) {
                        case "8:30":
                            if (position == 3) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "9:30":
                            if (position == 9) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "10:30":
                            if (position == 15) {
                                setUpTextViews(txt, ora);


                            }
                            break;

                        case "11:30":
                            if (position == 21) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "12:30":
                            if (position == 27) {
                                setUpTextViews(txt, ora);

                            }

                            break;

                        case "13:30":
                            if (position == 33) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "14:30":
                            if (position == 39) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "15:30":
                            if (position == 45) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "16:30":
                            if (position == 51) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "17:30":
                            if (position == 57) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "18:30":
                            if (position == 63) {
                                setUpTextViews(txt, ora);

                            }
                            break;
                    }
                    break;

                case 4:

                    switch (ora.oraInizio) {
                        case "8:30":
                            if (position == 4) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "9:30":
                            if (position == 10) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "10:30":
                            if (position == 16) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "11:30":
                            if (position == 22) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "12:30":
                            if (position == 28) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "13:30":
                            if (position == 34) {
                                setUpTextViews(txt, ora);


                            }
                            break;

                        case "14:30":
                            if (position == 40) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "15:30":
                            if (position == 46) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "16:30":
                            if (position == 52) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "17:30":
                            if (position == 58) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "18:30":
                            if (position == 64) {
                                setUpTextViews(txt, ora);

                            }
                            break;
                    }
                    break;

                case 5:

                    switch (ora.oraInizio) {
                        case "8:30":
                            if (position == 5) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "9:30":
                            if (position == 11) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "10:30":
                            if (position == 17) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "11:30":
                            if (position == 23) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "12:30":
                            if (position == 29) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "13:30":
                            if (position == 35) {
                                setUpTextViews(txt, ora);


                            }
                            break;

                        case "14:30":
                            if (position == 41) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "15:30":
                            if (position == 47) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "16:30":
                            if (position == 53) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "17:30":
                            if (position == 59) {
                                setUpTextViews(txt, ora);

                            }
                            break;

                        case "18:30":
                            if (position == 65) {
                                setUpTextViews(txt, ora);


                            }
                            break;
                    }
                    break;
            }
        }
    }

    /**
     * Metodo per la stampa del testo all'interno delle textview del calendario
     *
     * @param txt textview da "riempire"
     * @param ora fascia oraria selezionata
     */
    public void setUpTextViews(TextView txt, Orario ora) {
        txt.setBackgroundColor(Color.parseColor(ora.insegnamento.getColore()));
        txt.setText(ora.insegnamento.getNome());
        txt.setTextSize(10);
        txt.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
    }

    /**
     * Metodo per la creazione del calendario aggiornato con gli scambi confermati
     */
    public void joinCalendario() {
        // Dobbiamo prendere la lista che contiene il calendario permanente e dobbiamo inserire al suo interno gli scambi
        Log.e("ds", "InizioDestinatario");
        for (int i = 0; i < scambio.size(); i++) {
            Log.e("ds", scambio.get(i).giorno + "" + scambio.get(i).insegnamento.getNome() + "" + scambio.get(i).oraInizio + "" + i);
        }

        // ArrayList aggiornata
        calendarioConScambi = new ArrayList<>();
        int counter = 0;

        // Itera fino all'esaurimento della lista
        //Per ogni scambio verifichiamo dove deve essere posto all'interno della lista

        for (int s = 0; s < scambio.size(); s++) {

            for (int i = 0; i < lista.size(); i++) {
                if (lista.get(i).equals(scambio.get(s))) {

                    Log.e("no", "scambios" + scambio.get(s).giorno + "/" + scambio.get(s).oraInizio);
                    lista.set(i, scambio.get(s));

                }

            }


        }


    }
}
