package it.uniba.di.orariolezioni;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import it.uniba.di.orariolezioni.helper.HttpJsonParser;

/**
 * Classe che permette la visualizzazione degli scambi relativi al professore loggato. Tali scambi vengono recuperati
 * da remoto tramite metodi che estendono AsyncTask. In seguito all'ottenimento di questi dati viene caricato l'AdapterScambio
 * che si occupa della visualizzazione. Gli scambi sono aggiornati a runtime.
 */
public class FragmentScambio extends Fragment {

    public static boolean flag = true;

    SharedPreferences preferenzecondivise;
    public static final String PREFERENZE = "PreferenzePersonali";

    public FragmentScambio() {
        // Required empty public constructor
    }

    private class Risposta extends AsyncTask<Void,Void, JSONArray> {

        private final String GETSCAMBIO = "http://www.orariolezionisms.altervista.org/getScamvio.php";
        Map<String, String> mappa = new HashMap<>();

        private Risposta(String... param) {
            mappa.put("IdProfessore", param[0].trim());
        }

        @Override
        protected JSONArray doInBackground(Void... param) {
            try {

                HttpJsonParser parser = new HttpJsonParser();
                JSONArray jObj;
                jObj = parser.makeHttpRequest(GETSCAMBIO, "GET", mappa);
                return jObj;

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_fragment_scambio, container, false);
        preferenzecondivise = this.getActivity().getSharedPreferences(PREFERENZE, Context.MODE_PRIVATE);
        String IdProfessore = preferenzecondivise.getString("IdProfessore", String.valueOf(0));
        FragmentScambio.Risposta risp = new Risposta(IdProfessore);
        ListView ListaScambi = rootView.findViewById(R.id.ListaScambi);
        JSONArray arrayobj = null;

        try {
            arrayobj = risp.execute().get();
        } catch (ExecutionException e ) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            AdapterScambio adapterScambio = new AdapterScambio(arrayobj, this.getActivity().getApplicationContext());
            ListaScambi.setAdapter(adapterScambio);
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {
            onResume();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }
        if (!flag){
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
            flag = true;
        }
    }
}