package it.uniba.di.orariolezioni.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import it.uniba.di.orariolezioni.FragmentCalendario;
import it.uniba.di.orariolezioni.FragmentCronologia;
import it.uniba.di.orariolezioni.FragmentScambio;
import it.uniba.di.orariolezioni.MainActivity;
import it.uniba.di.orariolezioni.RichiestaScambio;
import it.uniba.di.orariolezioni.utils.NotificationUtils;
import it.uniba.di.orariolezioni.vo.NotificationVO;

/**
 * Gestione notifiche e token per la registrazione a firebase
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgingService";
    private static final String TITLE = "title";
    private static final String EMPTY = "";
    private static final String MESSAGE = "message";
    private static final String IMAGE = "image";
    private static final String ACTION = "action";
    private static final String DATA = "data";
    private static final String ACTION_DESTINATION = "action_destination";
    private static final String TOPIC_GLOBAL = "global";
    SharedPreferences preferenzecondivise;
    public static final String PREFERENZE = "PreferenzePersonali";


    public MyFirebaseMessagingService(){}

    /**
     * Ottenimento del token + registrazione
     * @param s token
     */
    @Override
    public void onNewToken(String s){
        super.onNewToken(s);
        sendRegistrationToServer(s);
        FirebaseMessaging.getInstance().subscribeToTopic(TOPIC_GLOBAL);
    }

    public void sendRegistrationToServer(String token){}

    /**
     * Azione da eseguire alla ricezione della notifica
     * @param remoteMessage messaggio
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage){

        super.onMessageReceived(remoteMessage);

        FragmentScambio.flag = false;
        FragmentCronologia.flag = false;
        RichiestaScambio.flag = false;
        FragmentCalendario.flag  = false;
        Log.d(TAG,"DA: " + remoteMessage.getFrom());

        if (remoteMessage.getData().size()>0){
            Log.d(TAG,"Data payload " + remoteMessage.getData());
            Map<String, String> data = remoteMessage.getData();
            handleData(data);
        } else if (remoteMessage.getNotification() != null){
            Log.d(TAG,"Notifica: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification());
        }
    }

    /**
     * Gestione notifica
     * @param RemoteMsgNotification messaggio remoto
     */
    private void handleNotification(RemoteMessage.Notification RemoteMsgNotification){

        String message = RemoteMsgNotification.getBody();
        String title = RemoteMsgNotification.getTitle();
        NotificationVO notificationVO = new NotificationVO();
        notificationVO.setTitle(title);
        notificationVO.setMessage(message);

        Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        notificationUtils.displayNotification(notificationVO,resultIntent);
        notificationUtils.playNotificationSound();
    }

    /**
     * Gestione dati notifica
     * @param data dati ricevuti
     */
    private void handleData(Map<String, String> data){
        String title = data.get(TITLE);
        String message = data.get(MESSAGE);
        String iconUrl = data.get(IMAGE);
        String action = data.get(ACTION);
        String actionDestination = data.get(ACTION_DESTINATION);
        NotificationVO notificationVO = new NotificationVO();
        notificationVO.setTitle(title);
        notificationVO.setMessage(message);
        notificationVO.setIconUrl(iconUrl);
        notificationVO.setAction(action);
        notificationVO.setActionDestination(actionDestination);

        Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        notificationUtils.displayNotification(notificationVO, resultIntent);
        notificationUtils.playNotificationSound();
    }

}
