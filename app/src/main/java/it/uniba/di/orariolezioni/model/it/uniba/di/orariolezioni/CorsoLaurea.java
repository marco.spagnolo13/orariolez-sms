package it.uniba.di.orariolezioni;

import android.os.Parcel;

import java.io.Serializable;
import java.util.ArrayList;

public class CorsoLaurea implements Serializable {

        private int idCorso;
        private String nomeCorso;
        private ArrayList<Insegnamento> insegnamento;

    public CorsoLaurea(int idCorso,String nomeCorso,ArrayList<Insegnamento> insegnamento){
        this.idCorso = idCorso;
        this.nomeCorso= nomeCorso;
        this.insegnamento = insegnamento;
    }

    public CorsoLaurea(int idCorso,String nomeCorso){
        this.idCorso = idCorso;
        this.nomeCorso= nomeCorso;
        this.insegnamento = new ArrayList<>();
    }

    protected CorsoLaurea(Parcel in) {
        idCorso = in.readInt();
        nomeCorso = in.readString();
        in.readArrayList(null);
    }

    public int getIdCorso(){
            return idCorso;
    }

    public String getNomeCorso(){
        return nomeCorso;
    }

    public ArrayList<Insegnamento> getInsegnamento(){
        return insegnamento;
    }

    public void setInsegnamento(Insegnamento insegnamento){
        this.insegnamento.add(insegnamento);
    }

    public void setInsegnamento(ArrayList<Insegnamento> insegnamento){
        this.insegnamento = insegnamento;
    }
}

