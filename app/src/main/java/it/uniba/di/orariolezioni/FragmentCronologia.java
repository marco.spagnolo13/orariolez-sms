package it.uniba.di.orariolezioni;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import it.uniba.di.orariolezioni.helper.HttpJsonParser;


/**
 * Classe che permette di ottenere da remoto gli scambi per i quali non è stata ancora effettuata una scelta. Gli scambi
 * vengono recuperati da remoto tramite un metodo che estende AsyncTask. La visualizzazione di questi dati è affidata
 * all'AdapterCronologia. La cronologia viene aggiornata a runtime tramite interfaccia.
 */
public class FragmentCronologia extends Fragment implements AdapterCronologia.AdapterInterface {

    public static boolean flag = true;

    SharedPreferences preferenzecondivise;
    private TextView data;
    public static final String PREFERENZE = "PreferenzePersonali";

    /**
     * Metodo utilizzato per ricaricare il fragment
     * Utilizza android.support.v4.app
     */
    @Override
    public void buttonPressed() {
        FragmentCronologia fragmentCronologia = this;
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.detach(fragmentCronologia);
        fragmentTransaction.attach(fragmentCronologia);
        fragmentTransaction.commit();
    }


    public FragmentCronologia() {
        // Construttore pubblico necessario vuoto
    }

    /**
     * Classe per ottenere i dati da database remoto
     *
     * @return JSONArray jObj array di oggetti json contenente il contenuto della query
     */
    private class Risposta1 extends AsyncTask<Void, Void, JSONArray> {

        private final String GETSCAMBIO = "http://www.orariolezionisms.altervista.org/getCronologva.php";
        Map<String, String> mappa = new HashMap<>();

        private Risposta1(String... param) {
            mappa.put("IdProfessore", param[0].trim());
        }

        @Override
        protected JSONArray doInBackground(Void... param) {
            try {

                HttpJsonParser parser = new HttpJsonParser();
                JSONArray jObj;
                jObj = parser.makeHttpRequest(GETSCAMBIO, "GET", mappa);
                return jObj;

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflater
        final View rootView = inflater.inflate(R.layout.fragment_fragment_cronologia, container, false);
        preferenzecondivise = this.getActivity().getSharedPreferences(PREFERENZE, Context.MODE_PRIVATE);
        String IdProfessore = preferenzecondivise.getString("IdProfessore", String.valueOf(0));
        FragmentCronologia.Risposta1 risposta = new Risposta1(IdProfessore);

        FirebaseMessaging.getInstance().subscribeToTopic(IdProfessore);

        ListView listacronologia = rootView.findViewById(R.id.ListaCronologia);
        JSONArray arrayobj = null;

        // Blocco try-catch per lanciare la Async
        try {
            arrayobj = risposta.execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Blocco try-catch per il caricamento dell'adapter
        try {
            AdapterCronologia adapterCronologia = new AdapterCronologia(arrayobj, this.getActivity().getApplicationContext(), this);
            listacronologia.setAdapter(adapterCronologia);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {
            onResume();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }
        if (!flag){
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
            flag = true;
        }
    }
}