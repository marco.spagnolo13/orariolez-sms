package it.uniba.di.orariolezioni;

public class Lezione {

    enum Giorni{
        Lunedi,Martedi,Mercoledi,Giovedi,Venerdi;
    }

    private Professore professore;
    private Insegnamento insegnamento;
    private float oraInizio;
    private float oraFine;
    private String aula;
    private Giorni giornoSettimana;

    private Lezione(Professore professore, Insegnamento insegnamento, float oraInizio, float oraFine,
                    String aula,Giorni giornoSettimana){
        this.professore = professore;
        this.insegnamento = insegnamento;
        this.oraFine = oraFine;
        this.oraInizio = oraInizio;
        this.aula = aula;
        this.giornoSettimana = giornoSettimana;
    }

    public Professore getProfessore(){ return professore; }
    public Insegnamento getInsegnamento(){return insegnamento;}
    public float getOraInizio(){ return oraInizio; }
    public float getOraFine(){ return oraFine; }
    public String getAula(){ return aula; }
    public Giorni getGiornoSettimana(){ return giornoSettimana;}

    public boolean equals(Lezione lezione){
        if(professore.equals(lezione.getProfessore())&&
                insegnamento.equals(lezione.insegnamento)
                && giornoSettimana.equals(lezione.giornoSettimana)){
            return true;
        } else {
            return false;
        }
    }
}
