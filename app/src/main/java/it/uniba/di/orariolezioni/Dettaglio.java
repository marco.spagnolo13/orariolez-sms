package it.uniba.di.orariolezioni;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Classe che contiene le materie insegnate da ogni professore.
 */
public class Dettaglio extends AppCompatActivity {

    TextView materia;
    TextView professore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dettaglio);
        materia = findViewById(R.id.NomeMateria);
        professore = findViewById(R.id.professore);
        Intent intent = getIntent();
        String nome = intent.getStringExtra("Materia");
        String prof = intent.getStringExtra("Professore");
        materia.setText(nome);
        professore.setText(prof);
    }
}
