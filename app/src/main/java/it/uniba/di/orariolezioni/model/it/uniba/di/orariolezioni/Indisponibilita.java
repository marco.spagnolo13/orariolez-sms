package it.uniba.di.orariolezioni;

import it.uniba.di.orariolezioni.Lezione;
import it.uniba.di.orariolezioni.Professore;

public class Indisponibilita {

    private Professore professore;
    private float oraInizio;
    private float oraFine;
    private Lezione.Giorni giornoSettimana;

    public Professore getProfessore(){ return professore;}
    public float getOraInizio(){ return oraInizio;}
    public float getOraFine(){ return oraFine;}
    public Lezione.Giorni getGiornoSettimana(){ return giornoSettimana;}

}
