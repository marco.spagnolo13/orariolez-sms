package it.uniba.di.orariolezioni.vo;

/**
 * Construttore per le notifiche
 */
public class NotificationVO {

    // Campi
    private String title;
    private String message;
    private String iconUrl;
    private String action;
    private String actionDestination;

    // Metodi getter
    public String getTitle(){ return title; }

    public String getMessage(){ return message; }

    public String getIconUrl(){ return iconUrl; }

    public String getAction(){ return action; }

    public String getActionDestination(){ return actionDestination; }

    // Metodi setter
    public void setTitle(String title){ this.title = title; }

    public void setMessage(String message){ this.message = message; }

    public void setIconUrl(String iconUrl){ this.iconUrl = iconUrl; }

    public void setAction(String action){ this.action = action; }

    public void setActionDestination(String actionDestination){ this.actionDestination = actionDestination; }

}
