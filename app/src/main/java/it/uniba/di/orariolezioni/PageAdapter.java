package it.uniba.di.orariolezioni;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Classe utile per caricare in memoria i 4 Fragment presenti all'interno dell'app. E' stato necessario introdurre
 * tale classe per evitare fenomeni di buffering o di dati inconsistenti
 */
public class PageAdapter extends FragmentStatePagerAdapter {

    private int numoftabs;
    private Context mcontext;

    public PageAdapter(FragmentManager fm,int numoftabs,Context mcontext) {
        super(fm);
        this.numoftabs = numoftabs;
        this.mcontext = mcontext;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: return new FragmentScambio();
            case 1: return new FragmentCalendario();
            case 2: return new FragmentCronologia();
            case 3: return new RichiestaScambio();
            default: return null;
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0: return mcontext.getResources().getString(R.string.scambio);
            case 1: return mcontext.getResources().getString(R.string.calendario);
            case 2: return mcontext.getResources().getString(R.string.cronologia) ;
            case 3: return mcontext.getResources().getString(R.string.richiesta);
            default: return null;
        }
    }
}