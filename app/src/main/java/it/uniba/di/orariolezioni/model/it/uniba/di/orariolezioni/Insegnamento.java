package it.uniba.di.orariolezioni;

import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

public class Insegnamento implements Serializable {

    private int id;
    private String nome;
    private String anno;
    private int cfu;
    private String semestre;
    private String gruppo;
    private String colore;
    private  String sigla;

    public Insegnamento(int id,String nome,String colore){
        this.id = id;
        this.nome = nome;
        this.colore = colore;
    }

    public Insegnamento(int id,String nome,String anno,int cfu,String semestre,String gruppo,String colore){
        this.id = id;
        this.nome = nome;
        this.anno = anno;
        this.cfu = cfu;
        this.semestre = semestre;
        this.gruppo = gruppo;
        this.colore = colore;
    }

    public Insegnamento(int id,String nome,String anno,int cfu,String semestre,String gruppo,String colore,String sigla){
        this.id = id;
        this.nome = nome;
        this.anno = anno;
        this.cfu = cfu;
        this.semestre = semestre;
        this.gruppo = gruppo;
        this.colore = colore;
        this.sigla = sigla;
    }


    public Insegnamento(int id,String nome){
        this.id = id;
        this.nome = nome;
    }

    // Metodi getter
    public int getId(){ return id; }
    public String getNome(){ return nome; }
    public String getAnno(){ return anno; }
    public int getCfu(){ return cfu; }
    public String getSemestre(){ return semestre; }
    public String getColore(){return  colore;}
    public String getGruppo(){return  gruppo;}
    public String getSigla(){return sigla;}
    // Metodi setter
    public void setSemestre(String semestre){ this.semestre = semestre; }
    public void setCfu(int cfu){ this.cfu = cfu; }
    public void setAnno(String anno){ this.anno = anno; }
    public void setNome(String nome){this.nome = nome;}

    public boolean equals(Insegnamento materia){
        if(id == materia.id){
            return true;
        } else {
            return false;
        }
    }
}
