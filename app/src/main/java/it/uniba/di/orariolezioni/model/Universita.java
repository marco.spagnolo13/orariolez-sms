import java.util.HashSet;

import it.uniba.di.orariolezioni.CorsoLaurea;
import it.uniba.di.orariolezioni.Professore;

public class Universita {

    private int id;
    private final String nome;
    private HashSet<CorsoLaurea> corsiLaurea = new HashSet<>();
    private HashSet<Professore> professori =  new HashSet<>();

    private Universita(int id,String nome){
        this.id = id;
        this.nome = nome;
    }

    public int getIdUniversita(){
        return id;
    }

    public String getNomeUniversita(){ return nome; }

    public void aggiungiCorso(CorsoLaurea corso){ corsiLaurea.add(corso); }

    public void rimuoviCorso(CorsoLaurea corso){
        if(corsiLaurea.contains(corso)){
            corsiLaurea.remove(corso);
        }
    }
}
