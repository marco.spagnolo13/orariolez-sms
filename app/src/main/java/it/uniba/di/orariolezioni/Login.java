package it.uniba.di.orariolezioni;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import it.uniba.di.orariolezioni.helper.HttpJsonParser;

/**
 * Classe che permette il login e preserva l'informazione dell'utente loggato utilizzando l'id legato a ogni
 * utente.
 */
public class Login extends AppCompatActivity {

    EditText usernameEditText, passwordEditText;
    Button submitLogin;
    Intent intentlogin;

    public static final String PREFERENZE = "PreferenzePersonali";
    public static final String Username = "NomeUtente";
    public static final String Password = "PasswordUtente";
    private final String VERIFICAACCOUNT = "http://www.orariolezionisms.altervista.org/verificaAccount.php";
    SharedPreferences preferenzecondivise;

    private class Risposta extends AsyncTask<Void, Void, JSONArray> {
        Map<String, String> mappa = new HashMap<>();

        private Risposta(String... param) {
            mappa.put("Email", param[0].trim());
            mappa.put("Password", param[1].trim());
        }

        @Override
        protected JSONArray doInBackground(Void... param) {

            try {
                HttpJsonParser parser = new HttpJsonParser();
                JSONArray jObj;
                jObj = parser.makeHttpRequest(VERIFICAACCOUNT, "GET", mappa);
                return jObj;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        preferenzecondivise = getSharedPreferences(PREFERENZE, Context.MODE_PRIVATE);

        usernameEditText = findViewById(R.id.usernameEditText);
        passwordEditText = findViewById(R.id.passwordEditText);
        submitLogin = findViewById(R.id.button);

        if (preferenzecondivise.getBoolean("Loggato", false)) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }

        submitLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vista) {

                // Raccolta informazioni di login
                String user = usernameEditText.getText().toString();
                String pass = passwordEditText.getText().toString();

                Risposta risp = new Risposta(user, pass);

                try {
                    JSONArray obj = risp.execute().get();
                    if (obj != null) {

                        JSONObject obje = obj.getJSONObject(0);
                        String risultato = obje.getString("IdUtente");
                        SharedPreferences.Editor editor = preferenzecondivise.edit();
                        editor.putString("IdProfessore", risultato);
                        editor.commit();
                        editor.putBoolean("Loggato", true);
                        editor.commit();

                        intentlogin = new Intent(Login.this, MainActivity.class);
                        startActivity(intentlogin);
                        finish();

                    } else {

                        Toast.makeText(Login.this, "Nome utente o password errati", Toast.LENGTH_LONG).show();
                        usernameEditText.setText("");
                        passwordEditText.setText("");

                    }
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    Toast.makeText(Login.this, "Nome utente o password errati", Toast.LENGTH_LONG).show();
                    usernameEditText.setText("");
                    passwordEditText.setText("");
                }
            }
        });
    }
}
