package it.uniba.di.orariolezioni;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Adapter relativo agli scambi. Permette la visualizzazione formattata degli scambi inserendo informazioni
 * relative ai giorni scambiati o allo stato dello scambio (con stato si intende se in attesa, confermato o rifiutato)
 */
public class AdapterScambio extends BaseAdapter implements ListAdapter {

    private ArrayList<ListaScambi> lista;
    TextView []textViews = new TextView[14];
    ArrayList<JSONObject> listascambi = new ArrayList<>();
    private boolean nodata;

    public class ListaScambi {

        int IdScambio;
        Professore Richiedente;
        Professore Destinatario;
        int giornoDest;
        int meseDest;
        int annoDest;
        String oraInizioDest;
        String oraFineDest;
        Insegnamento InsegnamentoRic;
        Insegnamento InsegnamentoDes;
        int permanente;
        int stato;
        int giornoRic;
        int meseRic;
        int annoRic;
        String oraInizioRic;
        String oraFineRic;

        public ListaScambi(int IdScambio, Professore Richiedente, Professore Destinatario, int giornoDest,
                           int meseDest, int annoDest, String oraInizioDest, String oraFineDest, Insegnamento InsegnamentoRic,
                           Insegnamento InsegnamentoDes, int permanente, int stato,
                           int giornoRic, int meseRic, int annoRic, String oraInizioRic, String oraFineRic) {
            this.IdScambio = IdScambio;
            this.Richiedente = Richiedente;
            this.Destinatario = Destinatario;
            this.giornoDest = giornoDest;
            this.meseDest = meseDest;
            this.annoDest = annoDest;
            this.oraInizioDest = oraInizioDest;
            this.oraFineDest = oraFineDest;
            this.InsegnamentoRic = InsegnamentoRic;
            this.InsegnamentoDes = InsegnamentoDes;
            this.permanente = permanente;
            this.stato = stato;
            this.giornoRic = giornoRic;
            this.meseRic = meseRic;
            this.annoRic = annoRic;
            this.oraInizioRic = oraInizioRic;
            this.oraFineRic = oraFineRic;
        }
    }

    JSONArray arrayobj;
    private Context mContext;

    public AdapterScambio(JSONArray arrayobj, Context mContext) {
        this.arrayobj = arrayobj;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return arrayobj.length();
    }

    @Override
    public Object getItem(int pos) {
        try {
            return arrayobj.get(pos);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    /**
     * Visualizzazione della listview con l'elenco di scambi richiesti e ricevuti dal prof
     * @param position posizione all'interno della list
     * @param convertView vista principale
     * @param parent vista parent
     * @return vista
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
            nodata = checkScambi();
            if (nodata) {
                view = inflater.inflate(R.layout.singolo_elemento_listview_scambi, null);
            } else {
                view = inflater.inflate(R.layout.noelement,null);
            }
        }

        if (nodata) {
            getScambiList();

            ImageView image;
            image = view.findViewById(R.id.imageView4);
            image.setImageResource(R.drawable.freccia);

            // Link textviews con elementi presenti su xml
            textViews[0] = view.findViewById(R.id.list_item_string);
            textViews[1] = view.findViewById(R.id.NomeCorsoRic);
            textViews[2] = view.findViewById(R.id.ProfRichiedenteScambio);
            textViews[3] = view.findViewById(R.id.OraInizioDest);
            textViews[4] = view.findViewById(R.id.OraFineDest);
            textViews[5] = view.findViewById(R.id.NomeCorsoDes);
            textViews[6] = view.findViewById(R.id.ProfDestinatarioScambio);
            textViews[7] = view.findViewById(R.id.GiornoDest);
            textViews[8] = view.findViewById(R.id.MeseDest);
            textViews[9] = view.findViewById(R.id.Stato);
            textViews[10] = view.findViewById(R.id.OraInizioRic);
            textViews[11] = view.findViewById(R.id.OraFineRic);
            textViews[12] = view.findViewById(R.id.GiornoRic);
            textViews[13] = view.findViewById(R.id.MeseRic);

            // Ciclo per il popolamento delle text views
            for (int i = 0; i < lista.size(); i++) {
                populateDataList(textViews, position, lista);
            }
        }
        return view;
    }

    /**
     * Controllo della presenza di dati in remoto
     * @return flag che indica la presenza di scambi o meno
     */
    public boolean checkScambi(){
        boolean flag = false;
        for (int i = 0; i < arrayobj.length(); i++){
            try {
                listascambi.add((JSONObject)arrayobj.get(i));
            } catch (JSONException e){
                e.printStackTrace();
            }
        }

        JSONObject scambi;
        for (int j = 0; j < listascambi.size(); j++){
            try {
                scambi = listascambi.get(j);
                if (Integer.parseInt(scambi.getString("risposta")) == 0){
                    flag = false;
                } else {
                    flag = true;
                }
            } catch (JSONException e){
                e.printStackTrace();
            }
        }

        return flag;
    }

    /**
     * Metodo per decodificare il JSONArray ricevuto e immetterlo in un arraylist
     */
    public void getScambiList() {

        for (int i = 0; i < arrayobj.length(); i++) {
            try {
                listascambi.add((JSONObject) arrayobj.get(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        JSONObject scambio;
        ListaScambi listaScambi;
        lista = new ArrayList<>();
        Professore Richiedente;
        Professore Destinatario;
        Insegnamento InsegnamentoRic;
        Insegnamento InsegnamentoDes;

        for (int i = 0; i < listascambi.size(); i++) {
            try {
                scambio = listascambi.get(i);
                Richiedente = new Professore(Integer.parseInt(scambio.getString("Richiedente")), scambio.getString("ProfRicNome"), scambio.getString("ProfRicCognome"));
                Destinatario = new Professore(Integer.parseInt(scambio.getString("Destinatario")), scambio.getString("ProfDestNome"), scambio.getString("ProfDestCognome"));
                InsegnamentoRic = new Insegnamento(Integer.parseInt(scambio.getString("InsegnamentoRichiedente")), scambio.getString("NomeInsegnamentoRic"));
                InsegnamentoDes = new Insegnamento(Integer.parseInt(scambio.getString("InsegnamentoDestinatario")), scambio.getString("NomeInsegnamentoDest"));
                listaScambi = new ListaScambi(Integer.parseInt(scambio.getString("IdScambio")), Richiedente, Destinatario, Integer.parseInt(scambio.getString("GiornoDest")),
                        Integer.parseInt(scambio.getString("MeseDest")), Integer.parseInt(scambio.getString("AnnoDest")), scambio.getString("FasciaInizialeDest"),
                        scambio.getString("FasciaFinaleDest"), InsegnamentoRic, InsegnamentoDes, Integer.parseInt(scambio.getString("Permanente")), Integer.parseInt(scambio.getString("Stato")),
                        Integer.parseInt(scambio.getString("GiornoRic")),Integer.parseInt(scambio.getString("MeseRic")),
                        Integer.parseInt(scambio.getString("AnnoRic")),scambio.getString("FasciaInizialeRic"),scambio.getString("FasciaFinaleRic"));
                lista.add(listaScambi);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Metodo per il caricamento dei dati ricevuti su textviews
     * @param tv contenente le textviews dichiarate interne alla listview
     * @param i posizione di ogni oggetto
     * @param list contenente i dati da stampare
     */
    public void populateDataList(TextView[] tv, Integer i, ArrayList<ListaScambi> list){
        tv[0].setText(String.valueOf(list.get(i).IdScambio));
        tv[1].setText(String.valueOf(list.get(i).InsegnamentoRic.getNome()));
        String appendNomeR = (list.get(i).Richiedente.getNome()) + " " + (list.get(i).Richiedente.getCognome());
        tv[2].setText(appendNomeR);
        tv[3].setText(String.valueOf(list.get(i).oraInizioDest));
        tv[4].setText(String.valueOf(list.get(i).oraFineDest));
        tv[5].setText(String.valueOf(list.get(i).InsegnamentoDes.getNome()));
        String appendNomeD = (list.get(i).Destinatario.getNome()) + " " + (list.get(i).Destinatario.getCognome());
        tv[6].setText(appendNomeD);
        tv[7].setText(String.valueOf(list.get(i).giornoDest));
        String appendMese = String.valueOf(list.get(i).meseDest);
        switch (appendMese) {
            case ("1"): tv[8].setText(R.string.gennaio);break;
            case ("2"): tv[8].setText(R.string.febbraio);break;
            case ("3"): tv[8].setText(R.string.marzo);break;
            case ("4"): tv[8].setText(R.string.aprile);break;
            case ("5"): tv[8].setText(R.string.maggio);break;
            case ("6"): tv[8].setText(R.string.giugno);break;
            case ("7"): tv[8].setText(R.string.luglio);break;
            case ("8"): tv[8].setText(R.string.agosto);break;
            case ("9"): tv[8].setText(R.string.settembre);break;
            case ("10"): tv[8].setText(R.string.ottobre);break;
            case ("11"): tv[8].setText(R.string.novembre);break;
            case ("12"): tv[8].setText(R.string.dicembre);break;
        }
        String stato = String.valueOf(list.get(i).stato);
        if (stato.equals("0")){
            tv[9].setText(R.string.attesa);
            int orange = Color.rgb(255, 165, 0);
            tv[9].setTextColor(orange);
        } else if (stato.equals("1")){
            tv[9].setText(R.string.confermato);
            tv[9].setTextColor(Color.GREEN);
        } else {
            tv[9].setText(R.string.rifiutato);
            tv[9].setTextColor(Color.RED);
        }
        tv[10].setText(String.valueOf(list.get(i).oraInizioRic));
        tv[11].setText(String.valueOf(list.get(i).oraFineRic));
        tv[12].setText(String.valueOf(list.get(i).giornoRic));
        String appendMeseRic = String.valueOf(list.get(i).meseRic);
        switch (appendMeseRic) {
            case ("1"): tv[13].setText(R.string.gennaio);break;
            case ("2"): tv[13].setText(R.string.febbraio);break;
            case ("3"): tv[13].setText(R.string.marzo);break;
            case ("4"): tv[13].setText(R.string.aprile);break;
            case ("5"): tv[13].setText(R.string.maggio);break;
            case ("6"): tv[13].setText(R.string.giugno);break;
            case ("7"): tv[13].setText(R.string.luglio);break;
            case ("8"): tv[13].setText(R.string.agosto);break;
            case ("9"): tv[13].setText(R.string.settembre);break;
            case ("10"): tv[13].setText(R.string.ottobre);break;
            case ("11"): tv[13].setText(R.string.novembre);break;
            case ("12"): tv[13].setText(R.string.dicembre);break;
        }
    }
}
