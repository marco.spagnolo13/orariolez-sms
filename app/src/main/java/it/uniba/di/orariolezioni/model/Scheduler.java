public class Scheduler {

    private final int id;
    private String nome;
    private String cognome;
    private String email;
    private String telefono;
    private String stanza;
    private boolean ruolo = true;

    private Scheduler(int id,String nome,String cognome,String email,String telefono,String stanza){
        this.id = id;
        this.nome = nome;
        this.cognome = cognome;
        this.email = email;
        this.telefono = telefono;
        this.stanza = stanza;
    }

    public int getId(){ return id; }
    public String getNome(){ return nome; }
    public String getEmail(){ return email;}
    public String getCognome(){ return cognome; }
    public String getTelefono(){ return telefono; }
    public String getStanza() { return stanza; }
    public boolean getRuolo() { return ruolo; }

    public boolean equals(Scheduler scheduler){
        if(id == scheduler.id){
            return true;
        } else {
            return false;
        }
    }
}
