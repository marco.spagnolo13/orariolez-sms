package it.uniba.di.orariolezioni;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import it.uniba.di.orariolezioni.helper.HttpJsonParser;

/**
 * Classe che permette di ottenere da remoto le informazioni utili per la creazione del calendario. Il calendario
 * visualizzato all'interno dell'adapter Calendario è composto da una GridView 9x11 che contiene nelle righe le
 * fasce orarie delle lezioni e nelle colonne i giorni della settimana da lunedi a venerdi. Ogni singola cella dove
 * stampare le informazioni relative alla lezione è gestita all'interno dell'Adapter calendario. Le informazioni relative
 * al calendario sono recuperate da remoto tramite metodo che estende AsyncTask. Vengono recuperate anche le materie insegnate
 * dal professore loggato per poter scegliere quale calendario visualizzare. Le materie sono poste all'interno di un
 * widget spinner. All'interno del calendario vi è anche un DatePickerDialog che permette di scegliere la settimana
 * da visualizzare. Vengono controllati anche eventuali scambi confermati per poter aggiornare coerentemente il calendario.
 */
public class FragmentCalendario extends Fragment implements DatePickerDialog.OnDateSetListener {

    // Aggiornamento del fragment
    public static boolean flag = true;

    private GridView grid;
    private Context mcontext;
    TextView lunedi;
    TextView martedi;
    TextView mercoledi;
    TextView giovedi;
    TextView venerdi;
    SharedPreferences preferenzecondivise;
    public static final String PREFERENZE = "PreferenzePersonali";
    private TextView data;

    private final String GETCALENDARIOPERMANENTE = "http://www.orariolezionisms.altervista.org/getCalendarioPermanente.php";
    int idCorsoLaurea = 0;
    int year;
    int mese;
    int day;
    int settimana;
    String anno;
    String gruppo;
    String semestre;
    int idInsegnamento;
    String sigla;

    public class InfoScambio {

        Professore professoreRichiedente;
        Professore professoreDestinatario;
        Insegnamento insegnamentoDestinatario;
        Insegnamento insegnamentoRichiedente;
        ArrayList<Orario> orarioD;
        ArrayList<Orario> orarioR;

        public InfoScambio(Professore professoreRichiedente, Professore professoreDestinatario,
                           Insegnamento insegnamentoDestinatario, Insegnamento insegnamentoRichiedente,
                           ArrayList<Orario> orarioD, ArrayList<Orario> orarioR) {

            this.professoreRichiedente = professoreRichiedente;
            this.professoreDestinatario = professoreDestinatario;
            this.insegnamentoDestinatario = insegnamentoDestinatario;
            this.insegnamentoRichiedente = insegnamentoRichiedente;
            this.orarioD = orarioD;
            this.orarioR = orarioR;
        }
    }

    /**
     * Metodo per ottenere info dei calendari da remoto
     */
    private class Risposta extends AsyncTask<Void, Void, JSONArray> {

        Map<String, String> mappa = new HashMap<>();

        private Risposta(String... param) {
            mappa.put("IdCorso", param[0].trim());
            mappa.put("Anno", param[1].trim());
            mappa.put("Semestre", param[3].trim());
            mappa.put("Gruppo", param[2]);
        }

        @Override
        protected JSONArray doInBackground(Void... param) {
            try {
                HttpJsonParser parser = new HttpJsonParser();
                JSONArray jObj;
                jObj = parser.makeHttpRequest(GETCALENDARIOPERMANENTE, "GET", mappa);
                return jObj;
            } catch (Exception e) {
                return null;
            }
        }
    }

    private final String GETSCAMBIOFROMCALENDARIO = "http://www.orariolezionisms.altervista.org/getScambioForCalendario.php";

    /**
     * Metodo per ottenere gli scambi di un determinato cdl con anno, semestre e settimana selezionati
     */
    private class VerificaScambio extends AsyncTask<Void, Void, JSONArray> {

        Map<String, String> mappa = new HashMap<>();

        private VerificaScambio(String... param) {
            mappa.put("Cdl", param[0].trim());
            mappa.put("Anno", param[1].trim());
            mappa.put("Semestre", param[2].trim());
            mappa.put("Settimana", param[3].trim());
            mappa.put("Gruppo",param[4].trim());
        }

        @Override
        protected JSONArray doInBackground(Void... param) {
            try {
                HttpJsonParser parser = new HttpJsonParser();
                JSONArray jObj;
                jObj = parser.makeHttpRequest(GETSCAMBIOFROMCALENDARIO, "GET", mappa);
                return jObj;
            } catch (Exception e) {
                return null;
            }
        }
    }

    public FragmentCalendario() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        String IdProfessore = null;
        preferenzecondivise = this.getActivity().getSharedPreferences(PREFERENZE, Context.MODE_PRIVATE);

        IdProfessore = preferenzecondivise.getString("IdProfessore", String.valueOf(0));
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_fragment_calendario, container, false);

        // Link con elementi xml
        grid = v.findViewById(R.id.grids);
        lunedi = v.findViewById(R.id.Lun);
        martedi = v.findViewById(R.id.Mar);
        mercoledi = v.findViewById(R.id.Mer);
        giovedi = v.findViewById(R.id.Gio);
        venerdi = v.findViewById(R.id.Ven);
        data = v.findViewById(R.id.giorno);

        String meseLettera = "";
        Calendar calendario = Calendar.getInstance();
        mese = calendario.get(Calendar.MONTH);
        day = calendario.get(Calendar.DAY_OF_MONTH);
        year = calendario.get(Calendar.YEAR);
        settimana = calendario.get(Calendar.WEEK_OF_YEAR);
        Spinner spinner = v.findViewById(R.id.spinner3);
        spinner.bringToFront();

        switch (mese) {
            case 0:  meseLettera += (R.string.gennaio); break;
            case 1:  meseLettera += getResources().getString(R.string.febbraio); break;
            case 2:  meseLettera += getResources().getString(R.string.marzo); break;
            case 3:  meseLettera += getResources().getString(R.string.aprile); break;
            case 4:  meseLettera += getResources().getString(R.string.maggio); break;
            case 5:  meseLettera += getResources().getString(R.string.giugno); break;
            case 6:  meseLettera += getResources().getString(R.string.luglio); break;
            case 7:  meseLettera += getResources().getString(R.string.agosto); break;
            case 8:  meseLettera += getResources().getString(R.string.settembre); break;
            case 9:  meseLettera += getResources().getString(R.string.ottobre); break;
            case 10: meseLettera += getResources().getString(R.string.novembre); break;
            case 11: meseLettera += getResources().getString(R.string.dicembre); break;
        }

        data.setText(day + " " + meseLettera);
        data.setTextColor(getResources().getColor(R.color.colorSecondary));

        data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                final DatePickerDialog dpd = DatePickerDialog.newInstance(FragmentCalendario.this,
                        now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
                dpd.setAccentColor(getResources().getColor(R.color.colorPrimaryDark));
                dpd.show(getActivity().getFragmentManager(), "Date picker");
            }
        });

        // Recuperiamo tutte le materie che insegna il professore;
        MainActivity activity = (MainActivity) getActivity();
        final Professore prof = activity.getProfLoggato();
        final ArrayList<CorsoLaurea> corsi = prof.getCorso();
        ArrayList<Insegnamento> materie = null;

        String[] elencoSpinner;

        for (int i = 0; i < corsi.size(); i++) {
            materie = corsi.get(i).getInsegnamento();
        }

        final ArrayList<Insegnamento> copia = materie;

        elencoSpinner = new String[materie.size()];
        for (int k = 0; k < materie.size(); k++) {
            elencoSpinner[k] = materie.get(k).getSigla();
        }

        // A questo punto possiamo creare un nuovo adapter per lo spinner
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(v.getContext(), android.R.layout.simple_list_item_1, elencoSpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                String selectedItem = adapterView.getItemAtPosition(position).toString();
                for (int i = 0; i < copia.size(); i++) {
                    if (selectedItem.equals(copia.get(position).getSigla())) {
                        for (int k = 0; k < corsi.size(); k++) {
                            ArrayList<Insegnamento> insegnamenti = corsi.get(k).getInsegnamento();
                            for (int f = 0; f < insegnamenti.size(); f++) {
                                if (insegnamenti.get(f).getSigla().equals(copia.get(position).getSigla())) {
                                    idCorsoLaurea = corsi.get(k).getIdCorso();
                                    break;
                                }
                            }
                            break;
                        }
                        anno = copia.get(position).getAnno();
                        gruppo = copia.get(position).getGruppo();
                        if (gruppo.isEmpty()) {
                            gruppo = "\"\"";
                        } else {
                            gruppo = "\"" + gruppo + "\"";
                        }
                        semestre = copia.get(position).getSemestre();
                        idInsegnamento = copia.get(position).getId();

                        // Prima di lanciare la richiesta al server vediamo se ci sono scambi
                        // Lanciamo una async alla tabella scambi per vedere se nella settimana della data inserita dal professore
                        // sono stati effettuati degli scambi
                        // TODO:mettere il semestre
                        VerificaScambio scambio = new VerificaScambio(idCorsoLaurea + "", anno + "", "1", settimana + "",gruppo);
                        JSONArray object = null;
                        try {
                            object = scambio.execute().get();
                            // Effettuiamo il parsing dei dati
                            parseScambio(object,settimana);
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
        return v;
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {
            onResume();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getUserVisibleHint()) {
            return;
        }
        if (!flag){
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
            flag = true;
        }
    }

    /**
     * Metodo per ottenere gli scambi della settimana selezionata
     * @param object Array oggetti JSON con scambi
     * @param settimana di interesse
     */
    public void parseScambio(JSONArray object,int settimana) {

        Risposta risp = new Risposta(idCorsoLaurea + "", anno, gruppo, semestre);

        JSONArray obj = null;
        try {
            obj = risp.execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Calendario cal = new Calendario(getActivity().getApplicationContext(), obj, grid, object,settimana);
        grid.setAdapter(cal);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        Calendar calendario = Calendar.getInstance();
        calendario.set(year, monthOfYear, dayOfMonth);
        int settimana = calendario.get(Calendar.WEEK_OF_YEAR);
        String meseLettera = "";
        switch (monthOfYear) {
            case 0:  meseLettera += (R.string.gennaio); break;
            case 1:  meseLettera += getResources().getString(R.string.febbraio); break;
            case 2:  meseLettera += getResources().getString(R.string.marzo); break;
            case 3:  meseLettera += getResources().getString(R.string.aprile); break;
            case 4:  meseLettera += getResources().getString(R.string.maggio); break;
            case 5:  meseLettera += getResources().getString(R.string.giugno); break;
            case 6:  meseLettera += getResources().getString(R.string.luglio); break;
            case 7:  meseLettera += getResources().getString(R.string.agosto); break;
            case 8:  meseLettera += getResources().getString(R.string.settembre); break;
            case 9:  meseLettera += getResources().getString(R.string.ottobre); break;
            case 10: meseLettera += getResources().getString(R.string.novembre); break;
            case 11: meseLettera += getResources().getString(R.string.dicembre); break;
        }

        data.setTextColor(getResources().getColor(R.color.colorSecondary));
        data.setText(dayOfMonth + "" + meseLettera);

        // TODO: mettere il semestre
        VerificaScambio scambio = new VerificaScambio(idCorsoLaurea + "", anno + "", "1", settimana + "",gruppo);
        JSONArray object = null;
        try {
            object = scambio.execute().get();
            //effettuiamo il parsing dei dati
            parseScambio(object,settimana);
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}